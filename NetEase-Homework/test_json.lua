local global = _G

--declare module
json = {}

--************************************************************
--utility functions for Unmarshal
local escapeList = {
      ['"'] = '\\"',
      ['\\'] = '\\\\',
      ['/'] = '\\/',
      ['\n'] = '\\n',
      ['\t'] = '\\t',
      ['\r'] = '\\r',
      ['\b'] = '\\b',
      ['\f'] = '\\f'      
}

-- deal with escape character
function encodeString(s)
         return s:gsub(".", function(c) return escapeList[c] end)
end


--return true if the obj is encodable, false if not
function isEncodable(obj)
         local t = global.type(obj)
         return (t == 'boolean' or t == 'number' or t == 'string' or t == 'nil' or t == 'table') or (t == 'function' and obj == null)
end

--judge whether the paramter is array or not
function isArray(t)
         local maxIdx = 0 
         for k, v in global.pairs(t) do
             if(global.type(k) == 'number' and math.floor(k) == k and k >= 1) then
                if (not isEncodable(v)) then
                   return false
                end
                
                maxIdx = math.max(maxIdx, k)
             else
                if (k == 'n') then
                   if v ~= table.getn(t) then
                      return false
                   end
                else
                   if isEncodable(v) then return false end     
                end -- end of if (k == 'n')                
             end -- end of if (type(k) ......)
         end -- end of for k, b
         return true, maxIdx
end
--************************************************************

--************************************************************
-- Main function 1:  parse lua string to json string format
function Unmarshal(lua_val)
         --deal with nil values
         if lua_val == nil then
            return "null"
         end

         --get the type of the parameter
         local v_type = global.type(lua_val)

         --deal with string
         if v_type == 'string' then
            return '"' .. encodeString(lua_val) .. '"'
         end

         --deal with bool
         if v_type == 'number' or v_type == 'boolean' then
            return global.tostring(lua_val)
         end

         --deal with table
         if v_type == 'table' then
            local ret = {}
            
            local isArray, maxCount = isArray(lua_val)
            
            --if lua_val is an array, we loop over all elements
            if isArray then
               for i = 1, maxCount do
                   table.insert(ret, Unmarshal(lua_val[i]))
               end
            --if lua_val isn't an array, but an object
            else
                for i, j in global.pairs(lua_val) do --loop over all pairs in the object, like "name" = "sunwei"
                    if isEncodable(i) and isEncodable(j) then
                       table.insert(ret, '"' .. encodeString(i) .. '":' .. Unmarshal(j))
                    end
                end
            end

            --empty lua table should be parsed into empty json object not array
            if isArray and maxCount ~= 0 then
               return '[' .. table.concat(ret, ',') .. ']'
            else
               return '{' .. table.concat(ret, ',') .. '}'
            end       
         end

         if v_type == 'function' and lua_val == null then
            return 'null'
         end

         global.print("unsupported type found when parse lua string to json string!")
end

--************************************************************
--utility functions for Marshal(json_str), parse json string to lua string

--skip all white spaces from current position
function marshal_skipWhitespace(s, startPos)
         local whitespace = " \n\r\t" -- four kinds of spaces
         local stringLen = string.len(s)
         while (string.find(whitespace, string.sub(s, startPos, startPos), 1, true) and startPos <= stringLen) do
         startPos = startPos + 1
         end
         return startPos
end

--decode an object
function marshal_scanObject(s, startPos)
         local object = {}
         local stringLen = string.len(s)
         local key, value
         if string.sub(s, startPos, startPos) ~= '{' then
            global.print("ERROR: wrong object position when marshal_scanObject!")
         end

         startPos = startPos + 1
         repeat
                --skip all spaces from current positions
                startPos = marshal_skipWhitespace(s, startPos)
                
                local curChar = string.sub(s, startPos, startPos)
                --return empty object, position move to the next char
                if (curChar == '}') then
                   return object, startPos + 1
                end

                --this happen because of repeat, decode the elements
                if (curChar == ',') then
                   startPos = marshal_skipWhitespace(s, startPos + 1)
                end
                
                --deal with keys
                key, startPos = Marshal(s, startPos) -- recursive call decode funtion
                if startPos > stringLen then
                   global.print("ERROR: Unexpected end when in function marshal_scanObject 111111")
                end
                                
                startPos = marshal_skipWhitespace(s, startPos)
                if startPos > stringLen then
                   global.print("ERROR: Unexpected end when in function marshal_scanObject 222222")
                end
                --global.print(string.sub(s, startPos, startPos))
                if string.sub(s, startPos, startPos) ~= ':' then
                   global.print("Error: Json string format found in function marshal_scanObject")
                end                    
                
                startPos = marshal_skipWhitespace(s, startPos + 1)
                if startPos > stringLen then
                   global.print("ERROR: Unexpected end when in function marshal_scanObject 333333")
                end
                
                value, startPos = Marshal(s, startPos)
                object[key] = value
         until false
end

--decode an array
function marshal_scanArray(s, startPos)
         local array = {}
         local stringLen = string.len(s)
         startPos = startPos + 1
         repeat
                startPos = marshal_skipWhitespace(s, startPos)
                local curChar = string.sub(s, startPos, startPos)
                if(curChar == ']') then
                     return array, startPos + 1
                end

                if(curChar == ',') then
                    startPos = marshal_skipWhitespace(s, startPos + 1)
                end
                object, startPos = Marshal(s, startPos)
                table.insert(array, object)
         until false
end

--decode a number
function marshal_scanNumber(s, startPos)
         local endPos = startPos + 1
         local stringLen = string.len(s)
         local accepttableChars = "+-0123456789.e"
         while (string.find(accepttableChars, string.sub(s, endPos, endPos), 1, true)
               and endPos <= stringLen)
               do
               endPos = endPos + 1
         end
         --global.print(s, startPos, endPos - 1)
         --local stringValue = 'return' .. string.sub(s, startPos, endPos - 1)
         --local stringEval = global.loadstring(stringValue)
         return string.sub(s, startPos, endPos - 1), endPos
         --if not stringEval then
            --global.print("Fail to scan number in function marshal_scanNumber!")
         --end
         --return stringEval(), endPos --here two values returned
end

--decode a string
function marshal_scanString(s, startPos)
         --global.print("******In marshal_scanString funtion")
         local startChar = string.sub(s, startPos, startPos)
         --global.print("startChar: " .. startChar)
         local t = {}
         local i, j = startPos, startPos
         while string.find(s, startChar, j + 1) ~= j + 1 do
               local oldj = j
               i, j = string.find(s, "\\.", j + 1)
               local x, y = string.find(s, startChar, oldj + 1)
               if not i or x < i then
                  --global.print("first", s, startPos, string.sub(s, startPos, oldj))
                  --global.print("above")
                  i, j = x, y - 1
                  if not x then global.print("secod", s, startPos, string.sub(s, startPos, oldj)) end
               end -- end of not i or x < i

               table.insert(t, string.sub(s, oldj + 1, i - 1))
               if string.sub(s, i, j) == "\\u" then
                  local a = string.sub(s, j + 1, j + 4)
                  j = j + 4
                  local n = global.tonumber(a, 16)

                  local x
                  if n < 0x80 then
                     x = string.char(n % 0x80)
                  elseif n < 0x800 then
                     x = string.char(0xC0 + (math.floor(n / 64) % 0x20), 0x80 + ( n % 0x40))
                  else
                     x = string.char(0xE0 + (math.floor(n / 4096) % 0x10), 0x80 + (math.floor(n / 64) % 0x40), 0x80 + (n % 0x40))
                  end
                  table.insert(t, x)
               else
                  table.insert(t, escapeList[string.sub(s, i, j)])
               end -- end of string.sub
         end -- end of while
         table.insert(t, string.sub(j, j + 1))
         --global.print("******Out of marshal_scanString funtion")         
         return table.concat(t, ""), j + 2
end

--decode a constant
function marshal_scanConstant(s, startPos)
         local consts = {["true"] = true, ["false"] = false, ["null"] = nil}
         local constNames = {"true", "false", "null"}

         for i, value in global.pairs(constNames) do
             --global.print ("[" .. string.sub(s,startPos, startPos + string.len(value) -1) .."]", value)
             if string.sub(s, startPos, startPos + string.len(value) - 1) == value then
                return consts[value], startPos + string.len(value)
             end
         end
end

--decode comments
function decode_scanComment(s, startPos)
         local endPos = string.find(s, '*/', startPos + 2)
         return endPos + 2
end
--************************************************************


--************************************************************
--main function 2: json string to lua string format
function Marshal(json_str, startPos)
         --if startPos is nil, set it to 1
         startPos = (startPos and startPos or 1)
         startPos = marshal_skipWhitespace(json_str, startPos)
         --global.print("startPos: " .. startPos)
         --global.print("string.len(json_str): " .. string.len(json_str))
         
         if startPos > string.len(json_str) then
            global.print("ERROR: out of range when decode string!")
         end

         local curChar = string.sub(json_str, startPos, startPos)
         --global.print("curChar: " .. curChar)
         
         --Object
         if curChar == '{' then
            return marshal_scanObject(json_str, startPos)
         end

         --Array
         --global.print("here: " .. startPos)
         if curChar == '[' then
            return marshal_scanArray(json_str, startPos)
         end

         --Number
         if string.find("+-0123456789.e", curChar, 1, true) then
            return marshal_scanNumber(json_str, startPos)
         end

         --string
         if curChar ==[["]] or curChar == [[']] then
            --global.print("Go into marshal_scanString function!")
            return marshal_scanString(json_str, startPos)
         end
         
         if string.sub(json_str, startPos, startPos + 1) == '/*' then
            return Marshal(json_str, decode_scanComment(json_str, startPos))
         end

         --if here, it should be a constant
         return marshal_scanConstant(json_str, startPos)         
end

json.Marshal = Marshal
json.Unmarshal = Unmarshal


global.print("##################test cases for Unmarshal, lua_str -> json_str##################")
global.print("******Test lua nil to json null******")
test_lua_nil = nil
global.print(Unmarshal(test_lua_nil))

global.print("******Test lua boolean to json boolean******")
test_lua_boolean_1 = true
global.print(Unmarshal(test_lua_boolean_1))
test_lua_boolean_2 = false
global.print(Unmarshal(test_lua_boolean_2))

global.print("******Test lua string to json string******")
test_lua_string_1 = "hello world"
global.print(Unmarshal(test_lua_string_1))
test_lua_string_2 = "special chars:\r\\*\b\f"
global.print(Unmarshal(test_lua_string_2))

global.print("******Test lua number to json number******")
test_lua_number_1 = 1.519283109283109238102938
global.print(Unmarshal(test_lua_number_1))
test_lua_number_2 = 7.65E8
global.print(Unmarshal(test_lua_number_2))
test_lua_number_3 = 7.65E-2
global.print(Unmarshal(test_lua_number_3))
test_lua_number_4 = 5E+8
global.print(Unmarshal(test_lua_number_4))
test_lua_number_5 = -0.3e10
global.print(Unmarshal(test_lua_number_5))

global.print("******Test lua empty table to json empty object, not empty array******")
test_lua_empty_table = {}
global.print(Unmarshal(test_lua_empty_table))

global.print("******Test lua array(table) to json array******")
test_lua_table_1 = {1, 2, 3, 4}
global.print(Unmarshal(test_lua_table))

global.print("******Test lua table to json object******")
test_lua_table_2 = {}
test_lua_table_2.name = "sunwei"
test_lua_table_2.class = "Barbarian"
test_lua_table_2.age = math.random(18, 28)
global.print(Unmarshal(test_lua_table_2))

global.print("******Test multi dimension lua table to json object******")
test_lua_table_3 = {}
test_lua_table_3.name = {}
test_lua_table_3.cost = {}
test_lua_table_3.name[1] = "Can Opener"
test_lua_table_3.cost[1] = "$12.75"
test_lua_table_3.name[2] = "Scissors"
test_lua_table_3.cost[2] = "$8.99"
global.print(Unmarshal(test_lua_table_3))

global.print("******Test combined lua table to json object******")
test_lua_combined_table = { 1, 2, 'fred', {first='mars',second='venus',third='earth'} }
global.print(Unmarshal(test_lua_combined_table))

global.print("******Requirement 5th test******")
test_5th_requirement = {nil, 1, nil, 1}
global.print(Unmarshal(test_5th_requirement))

--************************************************************
global.print(" ")
global.print("##################test cases for marshal, json_str -> lua_str##################")
global.print("******Test json null to lua nil******")
test_json_null = 'null'
test_json_null_ret = Marshal(test_json_null)
global.print(test_json_null_ret)

global.print("******Test json boolean to lua boolean******")
test_json_boolean_1 = 'true'
test_json_boolean_1_ret = Marshal(test_json_boolean_1)
global.print(test_json_boolean_1_ret)

test_json_boolean_2 = 'false'
test_json_boolean_2_ret = Marshal(test_json_boolean_2)
global.print(test_json_boolean_2_ret)

global.print("******Test json string to lua string******")
test_json_string_1 = '"hello world"'
test_json_string_1_ret = Marshal(test_json_string_1)
global.print(test_json_string_1_ret)
test_json_string_2 = '"special chars: %%\r\n\t"'
test_json_string_2_ret = Marshal(test_json_string_2)
global.print(test_json_string_2_ret)

global.print("******Test json number to lua number******")
test_json_number_1 = '-1.512319287319827192837'
test_json_number_1_ret = Marshal(test_json_number_1)
global.print(test_json_number_1_ret)
test_json_number_2 = '-0.3E+8'
test_json_number_2_ret = Marshal(test_json_number_2)
global.print(test_json_number_2_ret)
test_json_number_3 = '7.65e-2'
test_json_number_3_ret = Marshal(test_json_number_3)
global.print(test_json_number_3_ret)

global.print("******Test json array to lua table(array)******")
test_json_array = '[1, 2, 3, 4]'
test_json_array_ret = Marshal(test_json_array)
for key, value in global.pairs(test_json_array_ret) do
    global.print(value)
end

global.print("******Test json object to lua table******")
test_json_obj = '{"name" : "sunwei", "age" : 25}'
test_json_obj_ret = Marshal(test_json_obj)
for key, value in global.pairs(test_json_obj_ret) do
    global.print(key, value)
end

global.print("******Test json combined object to lua table******")
test_json_combined_obj = '{ "one":1, "two":2, "primes":[2,3,5,7] }'
test_json_combined_obj_ret = Marshal(test_json_combined_obj)
for key, value in global.pairs(test_json_combined_obj_ret) do
    global.print(key, value)
end
global.print("primes are: ")
for key, value in global.pairs(test_json_combined_obj_ret["primes"]) do
    global.print(value)
end


return json
