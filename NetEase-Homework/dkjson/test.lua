local json = require("json")

local global = _G
_G.Marshal = json.Marshal
_G.Unmarshal = json.Unmarshal


local tbl = -1.1231231823761237192313 
local str = Marshal(tbl)
global.print(str)


global.print("##################test cases for Unmarshal, lua_str -> json_str##################")
global.print("******Test lua nil to json null******")
test_lua_nil = nil
global.print(Unmarshal(test_lua_nil))

global.print("******Test lua boolean to json boolean******")
test_lua_boolean_1 = true
global.print(Unmarshal(test_lua_boolean_1))
test_lua_boolean_2 = false
global.print(Unmarshal(test_lua_boolean_2))

global.print("******Test lua string to json string******")
test_lua_string_1 = "hello world"
global.print(Unmarshal(test_lua_string_1))
test_lua_string_2 = "special chars:\r\\*\b\f"
global.print(Unmarshal(test_lua_string_2))

global.print("******Test lua number to json number******")
test_lua_number_1 = -42 --1.519283109283109238102938
global.print(Unmarshal(test_lua_number_1))
test_lua_number_2 = 7.65e8
global.print(Unmarshal(test_lua_number_2))
test_lua_number_3 = 7.65e-2
global.print(Unmarshal(test_lua_number_3))
test_lua_number_4 = 5e+8
global.print(Unmarshal(test_lua_number_4))
test_lua_number_5 = -0.3e10
global.print(Unmarshal(test_lua_number_5))

global.print("******Test lua empty table to json empty object, not empty array******")
test_lua_empty_table = {}
global.print(Unmarshal(test_lua_empty_table))

global.print("******Test lua array(table) to json array******")
test_lua_table_1 = {1, 2, 3, 4}
global.print(Unmarshal(test_lua_table))

global.print("******Test lua table to json object******")
test_lua_table_2 = {}
test_lua_table_2.name = "sunwei"
test_lua_table_2.class = "Barbarian"
test_lua_table_2.age = math.random(18, 28)
global.print(Unmarshal(test_lua_table_2))

global.print("******Test multi dimension lua table to json object******")
test_lua_table_3 = {}
test_lua_table_3.name = {}
test_lua_table_3.cost = {}
test_lua_table_3.name[1] = "Can Opener"
test_lua_table_3.cost[1] = "$12.75"
test_lua_table_3.name[2] = "Scissors"
test_lua_table_3.cost[2] = "$8.99"
global.print(Unmarshal(test_lua_table_3))

global.print("******Test combined lua table to json object******")
test_lua_combined_table = { 1, 2, 'fred', {first='mars',second='venus',third='earth'} }
global.print(Unmarshal(test_lua_combined_table))

global.print("******Requirement 5th test******")
test_5th_requirement = {nil, 1, nil, 1}
global.print(Unmarshal(test_5th_requirement))

--************************************************************
global.print(" ")
global.print("##################test cases for marshal, json_str -> lua_str##################")
global.print("******Test json null to lua nil******")
test_json_null = 'null'
test_json_null_ret = Marshal(test_json_null)
global.print(test_json_null_ret)

global.print("******Test json boolean to lua boolean******")
test_json_boolean_1 = 'true'
test_json_boolean_1_ret = Marshal(test_json_boolean_1)
global.print(test_json_boolean_1_ret)

test_json_boolean_2 = 'false'
test_json_boolean_2_ret = Marshal(test_json_boolean_2)
global.print(test_json_boolean_2_ret)

global.print("******Test json string to lua string******")
test_json_string_1 = '"hello world"'
test_json_string_1_ret = Marshal(test_json_string_1)
global.print(test_json_string_1_ret)
test_json_string_2 = '"special chars: %%\r\n\t"'
test_json_string_2_ret = Marshal(test_json_string_2)
global.print(test_json_string_2_ret)

global.print("******Test json number to lua number******")
test_json_number_1 = '-1.512319287319827192837'
test_json_number_1_ret = Marshal(test_json_number_1)
global.print(test_json_number_1_ret)
test_json_number_2 = '-0.3E+8'
test_json_number_2_ret = Marshal(test_json_number_2)
global.print(test_json_number_2_ret)
test_json_number_3 = '7.65e-2'
test_json_number_3_ret = Marshal(test_json_number_3)
global.print(test_json_number_3_ret)

global.print("******Test json array to lua table(array)******")
test_json_array = '[1, 2, 3, 4]'
test_json_array_ret = Marshal(test_json_array)
for key, value in global.pairs(test_json_array_ret) do
    global.print(value)
end

global.print("******Test json object to lua table******")
test_json_obj = '{"name" : "sunwei", "age" : 25}'
test_json_obj_ret = Marshal(test_json_obj)
for key, value in global.pairs(test_json_obj_ret) do
    global.print(key, value)
end

global.print("******Test json combined object to lua table******")
test_json_combined_obj = '{ "one":1, "two":2, "primes":[2,3,5,7] }'
test_json_combined_obj_ret = Marshal(test_json_combined_obj)
for key, value in global.pairs(test_json_combined_obj_ret) do
    global.print(key, value)
end
global.print("primes are: ")
for key, value in global.pairs(test_json_combined_obj_ret["primes"]) do
    global.print(value)
end

--global.print("******Test json unicode to lua ******")
--test_json_unicode = "\ulefa" --\u means four hex digits followed
--test_json_unicode_ret = Marshal(test_json_unicode)
--global.print(test_json_unicode_ret)
--global.print([[\xle\xfa]]) -- in lua \x means two hex digits followed



