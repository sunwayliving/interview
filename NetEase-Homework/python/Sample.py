from PyLuaTblParser import PyLuaTblParser
import string

def sample():
    parser = PyLuaTblParser()
    parser.loadLuaTable('test_case.txt')
    parser.dumpLuaTable('dump.txt')
    pass

sample()
