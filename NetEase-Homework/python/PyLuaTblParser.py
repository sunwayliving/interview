#coding=utf-8
#!/usr/bin/python

import string

class PyLuaTblParser(object):
    def __init__(self):
        self.dict = None

    #load data to the class
    def load(self, s):
        if not isinstance(s, str):
           raise Exception, 'not string'

        self.dict = None
        self.arr = []
        self.pos = 0
        self.str = s
        self.len = len(s)
        self.__tbl_next_token()
        pass

    #return the data in the class
    def dump(self):
        if not isinstance(self.dict, dict):
            raise Exception, 'not dict'
        self.arr=[]
        self.__dump_parse_dict(self.dict)
        return ''.join(self.arr)
        pass

    #load table data from file, f is the file path, call load method
    def loadLuaTable(self, f):
        self.arr, self.multi, self.endstr = [],False,''
        try:
            fobj = open(f,'r')
            for eachLine in fobj:
                self.arr.append(eachLine)
            fobj.close()
            self.load(''.join(self.arr))
            pass
        except IOError,e:
            raise 'file open error:', e

    #save lua table of the class to file, call dump method
    def dumpLuaTable(self, f):
        file_object = open(f,'w')
        try:
          luastr = self.dump()
          file_object.write(luastr)
        except IOError,e:
          raise 'file open error:',e

    #load data in the dict to the class
    #only handle number and string keys, ignore all other kinds of keys
    def loadDict(self, d):
        if not isinstance(d, dict):
            raise Exception, 'not dict'
        self.dict={}
        self.__copyDiction(d, self.dict)

    def dumpDict(self):
        if not self.dict:
            raise Exception,'selfdict is None'
        d={}
        self.__copyDiction(self.dict,d)
        return d

    #parse lua table number, ignore comments(begins with --)
    def __tbl_parse_num(self):
        last,ch=self.pos,self.str[self.pos]
        while '0'<=ch<='9' or ch=='e' or ch=='E' or ch=='.' or ch=='+' or ch=='-':
            if self.str[self.pos:self.pos+2]=='--':
                break
            self.pos+=1
            if self.pos>=self.len:
                break
            ch=self.str[self.pos]
        try:
            value = eval(self.str[last:self.pos])
        except Exception,e:
            raise Exception,e
        return value

    #parse string in the lua table,pay attention to special chars.
    #like: \ddd, ignore\u,\x,\/
    def __tbl_parse_string(self):
        escape2char = {'a' : '\a','r' : '\r','b' : '\b','f' : '\f','n' : '\n','t' : '\t',
           '\'' : '\'','"' : '"','v' : '\v','\\' : '\\'}
        arr=[]
        quote=self.str[self.pos]
        self.pos+=1
        while True:
            escpos=self.str.find('\\',self.pos)
            quotepos=self.str.find(quote,self.pos)
            if escpos==-1 and quotepos==-1:
                raise Exception,'error end string'
            if escpos>0 and ((quotepos==-1)or(quotepos>escpos)):
                nextpos=escpos
            else:
                nextpos=quotepos
            if nextpos>self.pos:
                arr.append(self.str[self.pos:nextpos])
                self.pos=nextpos
            if nextpos==quotepos:
                self.pos+=1
                break
            else:
                self.pos=nextpos+1
                escchar=self.str[self.pos]
                if escchar=='u' or escchar=='x':
                    raise Exception,'include u or x'
                elif escchar>='0' and escchar<='9':
                    endpos=self.pos+1
                    if self.str[self.pos+1]>='0' and self.str[self.pos+1]<='9':
                        if self.str[self.pos+2]>='0' and self.str[self.pos+2]<='9':
                            endpos=self.pos+3
                        else:
                            endpos=self.pos+2
                    value=chr(int(self.str[self.pos:endpos]))
                    self.pos=endpos
                else:
                    value=escape2char[escchar]
                    self.pos+=1
                arr.append(value)
        return ''.join(arr)
        pass


    def __tbl_insert(self,value):
        if isinstance(value,dict):
            last=len(self.arr)-2
            if self.arr[last]['type']=='dict':
                key=self.arr[last]['key']
                if key:
                    self.arr[last]['dict'][ key] =value['dict']
                    value['reverse_key']=key
                    self.arr[last]['key']=None
                else:
                   key=self.arr[last]['maxi']
                   self.arr[last]['dict'][key]=value['dict']
                   value['reverse_key']=key
                   self.arr[last]['maxi']+=1
            else:
                key=self.arr[last]['maxi']
                self.arr[last]['dict'][key]=value['dict']
                self.arr[last]['maxi']+=1
        else:
            cur=len(self.arr)-1
            key=self.arr[cur]['key']
            if self.arr[cur]['type']=='dict':
                if key:
                    if value!=None:
                        self.arr[cur]['dict'][ key] =value
                    self.arr[cur]['key']=None
                else:
                   key=self.arr[cur]['maxi']
                   self.arr[cur]['dict'][key]=value
                   self.arr[cur]['maxi']+=1
            elif self.arr[cur]['type']=='array':
               key=self.arr[cur]['maxi']
               self.arr[cur]['dict'][key]=value
               self.arr[cur]['maxi']+=1


    def __tbl_parse_tbl(self):
        flag=True

        if len(self.arr)==0:
            self.dict={}
            self.arr.append({'dict':self.dict,'key':None,'type':'array','maxi':1,'reverse_key':'root'})
        else:
            self.arr.append({'dict':{},'key':None,'type':'array','maxi':1,'reverse_key':None})
            self.__tbl_insert(self.arr[len(self.arr)-1])

        #skip '{'
        pos=self.str.find('}',self.pos)
        if pos<0:
            raise Exception,'error table struct'
        #handle key or value
        value=self.__tbl_next_token()
        if value=='}':
           flag=False
        while flag:
            #handle value
            if self.arr[len(self.arr)-1]['key']:
                value=self.__tbl_next_token()
            #handle comma
            value=self.__tbl_next_token()
            if value=='}':
                break
            if value!=',':
                raise Exception,'error table struct'

            #handle key or value
            value=self.__tbl_next_token()
            if value=='}':
                break

        if len(self.arr)>1:
            index=len(self.arr)-1
            last=index-1
            if self.arr[index]['type']=='array' and len(self.arr[index]['dict'])>0:
                reversekey=self.arr[index]['reverse_key']
                self.arr[last]['dict'][reversekey]=self.arr[index]['dict'].values()
            del self.arr[index]
        pass

    def __eat_white_space(self):
        whitespace={' ':1,'\t':1,'\r':1,'\n':1,'\f':1,'\v':1}
        while (self.pos<self.len) and (self.str[self.pos] in whitespace):
            self.pos+=1

        while self.pos<self.len:
            if self.str[self.pos:self.pos+3]=='--[':
                endarr,i=['--]'],self.pos+3
                while i<self.len and (self.str[i]=='=' or self.str[i]=='['):
                    if self.str[i]=='[':
                        endarr.append(']')
                        self.endstr=''.join(endarr)
                        break
                    endarr.append('=')
                    i+=1
                if not self.endstr:
                    self.pos=i-1
                    while(self.pos < self.len and self.str[self.pos] != "\n" ):
                         self.pos += 1
                    self.pos += 1
                else:
                    self.pos=self.str.find(self.endstr,self.pos)
                    if self.pos<0:
                         raise Exception
                    else:
                         self.pos+=len(self.endstr)

            elif self.str[self.pos:self.pos+2]=='--':
                self.pos += 2
                while(self.pos < self.len and self.str[self.pos] != "\n"):
                    self.pos += 1
                self.pos += 1
            else:
                 break
            while (self.pos<self.len) and (self.str[self.pos] in whitespace):
                self.pos+=1
        if self.pos>self.len:
            raise Exception


    def __tbl_parse_bracket(self):
        self.pos+=1#skip '['

        self.__eat_white_space()#eat whitespace
        ch=self.str[self.pos]
        if ch=='"' or ch=='\'':
            self.arr[len(self.arr)-1]['key']=self.__tbl_parse_string()#key
        elif ch=='+' or ch=='-' or '0'<=ch<='9':
            self.arr[len(self.arr)-1]['key']=self.__tbl_parse_num()
        else:
            raise Exception,'error table struct'

        self.__eat_white_space()#eat whitespace
        if self.str[self.pos]!=']':
            raise Exception,'error table struct'
        self.pos+=1

        self.__eat_white_space()#eat whitespace
        if self.str[self.pos]!='=':
            raise Exception,'error table struct'
        self.pos+=1

    def __tbl_next_token(self):
        value=None
        'Eat whitespace'
        self.__eat_white_space()
        ch = self.str[self.pos]
        if ch=='{':
            self.pos +=1
            self.__tbl_parse_tbl()
            return
        elif ch=='}' or ch==',':
            self.pos +=1
            return ch
        elif ch=='"' or ch=="'":
            value=self.__tbl_parse_string()
            self.__tbl_insert(value)
            return value
        elif ch=='+' or ch=='-' or (ch>='0'and ch<='9'):
            value=self.__tbl_parse_num()
            self.__tbl_insert(value)
            return value
        elif  ch=='[':
            self.arr[len(self.arr)-1]['type']='dict'
            self.__tbl_parse_bracket()
            return
        else:
            lastpos=self.pos

            while self.pos<self.len and (self.str[self.pos].isalnum()  or self.str[self.pos]=='_'):
                    self.pos+=1
            ch=self.str[lastpos:self.pos]
            self.__eat_white_space()
            if  self.str[self.pos]=='=':
                self.pos+=1
                self.arr[len(self.arr)-1]['type']='dict'
                self.arr[len(self.arr)-1]['key']=ch
                return
            else:
                if ch=='true':
                    value=True
                elif ch=='false':
                    value=False
                elif ch=='nil':
                    value=None
                else:
                    raise Exception,'error char'
                self.__tbl_insert(value)
                return value

    def __dump_parse_string(self,strVal):
        char2escape = {"\a":"\\a","\r":"\\r","\b":"\\b","\f":"\\f","\n":"\\n",
                    "\t":"\\t","'":"\\'","\"":"\\\"","\v":"\\v","\\":"\\\\",}
        buf=[]
        buf.append('"')
        if not isinstance(strVal, (str,unicode)):
            raise Exception, 'not str'
        for i in range(len(strVal)):
            if strVal[i] in char2escape:
                buf.append(char2escape[strVal[i]])
            else:
                buf.append(strVal[i])
        buf.append('"')
        return str(''.join(buf))

    def __dump_parse_list(self,arr):
        self.arr.append('{')
        if not isinstance(arr, list):
            raise Exception, 'not dict'
        for i in range(len(arr)):
            value = arr[i]
            if isinstance(value, dict):
                self.__dump_parse_dict(value)
            elif isinstance(value, list):
                self.__dump_parse_list(value)
            elif isinstance(value, (str,unicode)):
                self.arr.append(self.__dump_parse_string(value))
            else:
                self.__dump_parse_other(value)

            self.arr.append(',')
        self.arr.append('}')
        pass

    def __dump_parse_other(self,value):
        'parse boolean,none,num'
        transDict={'None':'nil','True':'true','False':'false'}
        if str(value) in transDict:
            self.arr.append(transDict[str(value)])
        elif isinstance(value, (int,float)):
            self.arr.append(str(value))
        else:
            raise Exception, 'not correct other data'

    def __dump_parse_dict(self,curDict):
        'parse dict'
        self.arr.append('{')

        self.arr.append('\n')
        if not isinstance(curDict, dict):
            raise Exception, 'not dict'
        for key in curDict:
            #key
            value=curDict[key]
            self.arr.append('[')
            if isinstance(key, (int,float)):
                self.arr.append(str(key))
            elif isinstance(key, str) or isinstance(key, unicode):
                self.arr.append(self.__dump_parse_string(key))
            else:
                raise Exception, 'not correct dict'
            self.arr.append(']')
            self.arr.append('=')

            #value
            if isinstance(value, dict):
                self.__dump_parse_dict(value)
            elif isinstance(value, list):
                self.__dump_parse_list(value)
            elif isinstance(value, str) or isinstance(value, unicode):
                self.arr.append(self.__dump_parse_string(value))
            else:
                self.__dump_parse_other(value)

            self.arr.append(',')
            self.arr.append('\n')
        self.arr.append('}')


    def __copyList(self,src,dst):
        for i in range(len(src)):
                if isinstance(src[i],list):
                    dst.append([])
                    self.__copyList(src[i],dst[len(dst)-1])
                elif isinstance(src[i], dict):
                    dst.append({})
                    self.__copyDiction(src[i],dst[len(dst)-1])
                else:
                    dst.append(src[i])

    def __copyDiction(self,src,dst):
        for key in src:
            if isinstance(key, (int,float,str,unicode)):
                if isinstance(src[key],list):
                    dst[key]=[]
                    self.__copyList(src[key],dst[key])
                elif isinstance(src[key], dict):
                    dst[key]={}
                    self.__copyDiction(src[key],dst[key])
                else:
                    dst[key]=src[key]
