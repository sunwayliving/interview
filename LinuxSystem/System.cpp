#include <iostream>
#include <cstdio>
#include <unistd.h>
using namespace std;

//ASCII  '0':48, 'a': 65, 'A':97
bool isLittleEnd(){
    unsigned short a = 0x1234;
    //char *b = (char*) &a;
    //printf("b: %x\n", *b);
    if(*((char*)(&a)) == 0x34)
        return true;
    else return false;

}

void learnFork(){
    int pid = fork();
    if(pid == 0){
        printf("ppid = %d, pid = %d\n", getppid(), getpid());
        cout<<"child" <<endl;
    }else{
        cout<<"pid: " <<pid<<endl;//parent process, pid equals child's pid
        printf("ppid = %d, pid = %d\n", getppid(), getpid());
        cout<<"parent" <<endl;
    }
}

//区别见：http://blog.csdn.net/shudaqi2010/article/details/24611675
void learnVFork(){
    int pid = vfork();
    if(pid == 0){
        printf("ppid = %d, pid = %d\n", getppid(), getpid());
        cout<<"child" <<endl;
    }else{
        cout<<"pid: " <<pid<<endl;//parent process, pid equals child's pid
        printf("ppid = %d, pid = %d\n", getppid(), getpid());
        cout<<"parent" <<endl;
    }
}

int main()
{
    learnVFork();
    return 0;
}
