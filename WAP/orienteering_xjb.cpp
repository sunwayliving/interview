/*    @Author:   Xin Junbo
*     @School:   University of Chinese Academy of Science
*     @Contact:  13168737278    newbossxin@126.com
*	   @Date:        29/09/2014
*******************************************************************************************
*     Solutions:
1) find all the  checkpoints '@' in the input matrix, save them in the checkpoints[20](at most 18 '@')
2) Using BFS algorithm to find the given two points' shortest distance, where
dist[i][j] records  the shortest distance between the checkpoints of i and  j(i =1,2, ..., checkNum; j =i+1, ..., checkNum) 
3) Using DP algorithm to find the shortest path from start point 'S' to goal point 'G' passing all the checkpoints 
dp[State][i] means in the current State, the shortest way to go to i checkpoints, where 0< State < (1<< 20)-1, the k-th
bit in  State denotes whether this bit has been visited or not.
For the state shift equation:
DP(State,i) = min{DP(State^(1<<i-1),k) + dis[k][j],DP(S,i)}, where S^(1<<i-1) means all the states that has not yet
visit the i checkpoints,  1<=k<=n  
*/

#include<iostream>
#include<queue>
using  namespace std;
const  int  MAXNUM = 100;
int       dp[1<<20][20];

class Orienteering{
public:
 struct pos{
	int         x, y;
	int         step;
	};
	void      main();
	int         input();  //return checkpoint numbers +2(start + goal)
	int         BFS(pos start, pos goal);  //return shortest distance of start and goal
	int         DP( int checkNum); //return final result

private:	 
	int          width, height;
	pos         checkpoints[20];
	int          dist[20][20];
	char        map[MAXNUM][MAXNUM];
};

void Orienteering::main(){
	int checkNum = input();
	if(checkNum == -1){
		cout << -1 << endl;      //Invalid input
		return;
	}
	for(int i = 0; i < checkNum; ++i){	     
		for(int j = i; j < checkNum; ++j){
			dist[i][j] = dist[j][i] = BFS(checkpoints[i], checkpoints[j]); 
			if(dist[i][j] == INT_MAX){
				cout << -1 << endl;  // Unable to reach the final goal point
				return;
			}
		}
	} //end for i
	int res = DP(checkNum);
	cout << res << endl;
}

int Orienteering::input(){
	int S_counts  = 0;
	int G_counts = 0;
	int C_counts = 1; //including start 'S', goal 'G' and checkpoints '@'
	cin >> width >> height;
	pos start,  goal;
	for(int i =0; i < height; ++i){       //y
		for(int j = 0; j < width; ++j){  // x
			cin >> map[j][i];
		}
	}
	//***Traversing the input, record the start 'S',  goal 'G' and checkpoints '@'***
	for(int i =0; i < height; ++i){
		for(int j =0; j < width; ++j){
			if(map[j][i] == 'S'){   // start 'S
				start.x = j;
				start.y = i;
				start.step = 0;
				++S_counts;
			}else if(map[j][i] == 'G'){ // goal point  'G'
				goal.x = j;
				goal.y = i;
				goal.step = 0;
				++G_counts;
			}else if(map[j][i] == '@'){  //checkpoints '@'
				checkpoints[C_counts].x = j;
				checkpoints[C_counts].y = i;
				checkpoints[C_counts].step = 0;
				++C_counts;
			}
		}// end col
	}// end row
	checkpoints[0] = start;
	checkpoints[C_counts++] = goal;
	if(S_counts != 1 || G_counts !=1 || C_counts >20)
		return -1;  // invalid input	      
	return C_counts;   //return total checkpoints, including start and goal points.
}

//Giving two points start and goal, find the shortest distance of them.
int Orienteering:: BFS(pos start, pos goal){
	bool isVisted[MAXNUM][MAXNUM];
	int dirX[4] ={0,-1,0,1};  //down, left, up, right
	int dirY[4] ={-1,0,1,0};
	memset(isVisted, false, sizeof(isVisted));
	queue<pos> path;
	path.push(start);
	isVisted[start.x][start.y] = true; 
	pos curP = start, temP;
	while( ! path.empty() ){
		curP = path.front();
		path.pop(); 
		if(curP.x == goal.x && curP.y == goal.y)
			return curP.step;
		//for the current position curP, search it in the 4 directions and push the valid path in the queue.
		for(int i = 0; i < 4; ++i){
			temP.x = curP.x + dirX[i];
			temP.y = curP.y + dirY[i];
			temP.step = curP.step +1;
			char test = map[temP.x][temP.y] ;
			if(temP.x >= 0 && temP.x < width &&  temP.y >=0 && temP.y < height 
				&& !isVisted[temP.x][temP.y] && map[temP.x][temP.y] != '#'){
					isVisted[temP.x][temP.y] = true;
					path.push(temP);
			}	 
		}
	}
	return INT_MAX;  // unreachable from start to goal
}

//******Giving the matrix dist[20][20], find the shortest way from start to goal*******
int Orienteering:: DP( int checkNum){
	if(checkNum == 2) 	return dist[0][1];
	//the 1st and last points of checkpoints[20] is start 'S' and goal 'G', so total number is checkNum
	for(int State = 0; State < (1<< (checkNum-2) ); ++State){
		for(int i =1; i <= checkNum-2; ++i ){
			if( State & (1 << (i-1)) ){  // current state has already visited the i checkpoints.
				if( State == (1 << (i-1)) ) //only visit the i checkpoints.
					dp[State][i] = dist[0][i];
				else{
					dp[State][i] = INT_MAX;
					for(int j =1; j <= checkNum-2; ++j){
						if(State & (1 << (j-1) ) && i != j ) // find the other cities
							dp[State][i] = min(dp[State ^ (1<<(i-1))][j] + dist[j][i], dp[State][i]);
					}
				}
			}
		} 
	}//end for State
	int   result = dp[(1<< (checkNum-2) )-1][1] + dist[1][checkNum-1]; //return to the goal, index is checkNum -1
	for(int m=2; m < checkNum-1; ++m)  
		result = min(result,dp[(1<< (checkNum-2))-1][m]+dist[m][checkNum-1]);  
	return result;  //shortest steps
}

int main(int argc, char* argv[]){
	Orienteering o;
	o.main();
	return 0;
}