﻿/* 
 * @file orienteering.cpp 
 * @brief implement the orienteering game, the aim is to arrive at the goal(G) from the
 *         start(S) with the shortest distance and pass all the checkpoints(G) on the map.
 * 
 * @solution
 *  step1: use bfs to find the shortest path between all pairs of points(checkpoints, start and goal)
 *  step2: use States Compressing DP to solve out the satisfied path 
 * 
 * @author zoomin (tencenthao@gmail.com, 15652200440) 
 * @date 2014/09/29 
 */
 

#include <iostream>
#include <queue>
#include <string>

const int N = 100;
const int K = 20;
//dp status
int DP[1<<K][K];

class Orienteering {
public:
	void	main();
private:
	void	readFile();       //read input and do initialization
	void	bfsMinDis();     //step1: bfs
	void	findPathbyDP();    //step2: states compressing
private:
	/*all points*/
	int		m_width;
	int		m_height;
	char	m_mapMat[N][N];     //input matrix
	/*check points*/
	int		m_checkPoints[K];     //index of checkpoints, S, G. Here transfer (i, j) to i * width + j
	int		m_dis[K][K];          //distance between all pairs of checkpoints, S, G
	int		m_cPNum;              //number of checkpoints, S, G
	/*error detection*/
	bool	m_isReachable;   //G could reach S and pass all checkpoints
	bool	m_isValid;   //1. G and S appear once  2. height, width, checkpoints satisfy 3. no other character
};

void Orienteering::main()
{
	readFile();
	if (m_isValid)
	{
		bfsMinDis();
		if (m_isReachable)
		{
			findPathbyDP();
		    return;
		}
	}
	std::cout<<-1<<std::endl;
}

void Orienteering::readFile()
{
	m_isValid = true;
	m_cPNum = 1;
	int tp_x = 0, tp_y = 0;
	int scount = 0, ecount = 0;

	std::cin>>m_width>>m_height;
	if (m_width > 100 || m_height > 100)
	{
		m_isValid = false;
	}

	char c;
	for (int i = 0; i < m_height; ++i)
	{
		for (int j = 0; j < m_width; ++j)
		{
			std::cin>>c;
			m_mapMat[i][j] = c;
			if (c == '@')
			{
				m_checkPoints[m_cPNum++] = i * m_width + j;
			}
			else if (c == 'S')
			{
				scount++;
				m_checkPoints[0] = i * m_width + j;    //put 'S' at the first pos
			}
			else if(c == 'G')
			{
				ecount++;
				tp_x = j;
				tp_y = i;
			}
			else if (c != '.' && c != '#')
			{
				m_isValid = false;
				return;
			}
		}
	}
	
	if (scount != 1 || ecount != 1 || m_cPNum > 20)
	{
		m_isValid = false;
		return;
	}
	m_checkPoints[m_cPNum++] = tp_y * m_width + tp_x;   //put 'G' at the last pos
	
	for (int i = 0; i < m_cPNum; i++)  
	{
		for (int j = 0; j < m_cPNum; j++)
		{
			m_dis[i][j] = INT_MAX;
		}
	}
}

void Orienteering::bfsMinDis()
{
	std::queue<int> que;
	bool visited[N][N]; 
	int dir[4][4] = {{1, 0}, {-1, 0}, {0, 1}, {0, -1}};
	for (int i = 0; i < m_cPNum; ++i)
	{
		memset(visited, false, sizeof(visited));
		/*bfs*/
		que.push(m_checkPoints[i] % m_width);
		que.push(m_checkPoints[i] / m_width);
		que.push(0);
		m_dis[i][i] = 0;
		visited[m_checkPoints[i] / m_width][m_checkPoints[i] % m_width] = true;
		while( !que.empty() )
		{
			int x = que.front();
			que.pop();
			int y = que.front();
			que.pop();
			int depth = que.front();
			que.pop();
			for (int k = 0; k < 4; k++)
			{
				int tmpx = x + dir[k][0];
				int tmpy = y + dir[k][1];
				if (tmpy < 0 || tmpy >= m_height || tmpx < 0 || tmpx >= m_width)
				{
					continue;
				}
				
				if( m_mapMat[tmpy][tmpx] != '#' && !visited[tmpy][tmpx] )
				{
					que.push(tmpx);
					que.push(tmpy);
					que.push(depth + 1);
					if (m_mapMat[tmpy][tmpx] != '.')
					{
						for(int j = 0; j < m_cPNum; ++j)
						{
							if(m_checkPoints[j] == tmpy * m_width + tmpx)
							{
								m_dis[i][j] = depth + 1;
								break;
							}
						}
					}
					visited[tmpy][tmpx] = true;
				}
			}
		}
	}
    /*check*/
	m_isReachable = true;
	for (int i = 0; i < m_cPNum; ++i)
	{
		for (int j = 0; j < m_cPNum; ++j)
		{
			if(m_dis[i][j] == INT_MAX)
			{
				m_isReachable = false;
				return;
			}
		}
	}
}

void Orienteering::findPathbyDP()
{
	for(int s = 0; s < ( 1 << m_cPNum ); s++)  
	{  
		for(int i = 0; i < m_cPNum; i++)  
		{  
			if( s & ( 1 << i ) )  
			{  
				if( s == ( 1 << i ) )  
				{
					DP[s][i] = m_dis[0][i]; 
				}
				else  
				{  
					DP[s][i] = INT_MAX;  
					for(int j = 0; j < m_cPNum; j++)  
					{  
						if( s & ( 1 << j ) && j != i )
						{
							DP[s][i] = std::min(DP[s][i], DP[s^(1 << i)][j] + m_dis[j][i]);  
						}
					}  
				}     
			}  
		}  
	}  
	int minLength = DP[( 1 << m_cPNum ) - 1][m_cPNum - 1];   //the cost when all the points are passed and located at G
	std::cout<<minLength<<std::endl;
}


int main(int argc, char* argv[])
{
	Orienteering o;
	o.main();
	return 0;
}