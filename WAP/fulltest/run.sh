#!/bin/bash
function runalltestinfile(){
    for file in `ls $1`
    do
        #if [ -f $file ]
        #then
        echo  "file name: $file"
        echo "haozhuming "; ./hzm < $1"/"$file >> hzm_result.txt
        echo "sunwei "; ./sw < $1"/"$file >> sw_result.txt
        echo "xinjunbo "; ./xjb < $1"/"$file >>xjb_result.txt
        echo "************************************************************"
        #fi
    done
}

if [ $# -lt 1 ]
then echo "输入文件名"
    exit -1
fi

if [ ! -d $1 ]
then echo "不是目录"
    exit -1
fi

filepath=`pwd`
if [ -f $filepath/sw_result.txt ]
then
    rm $filepath/sw_result.txt
fi

if [ -f $filepath/hzm_result.txt ]
then
    rm $filepath/hzm_result.txt
fi

if [ -f $filepath/xjb_result.txt ]
then
    rm $filepath/xjb_result.txt
fi

runalltestinfile $1
vimdiff sw_result.txt hzm_result.txt xjb_result.txt
