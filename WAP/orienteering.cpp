#include <iostream>
#include <string>
#include <queue>

/**
 * @author Sun Wei (sunwayliving @ gmail.com)
 * The solution is divided into two parts:
 * 1. Use breadth-first-search to calculate the minimum distance between every two points
 * 2. Use compressed state dynamic programming to compute the shortest path between Start and Goal
 *
 * I first used STL containers for readability, but the time cost is nearly 3 time as C style code, so I replaced
 * STL containers with array from C, which sacrifices some readability. And the perfomance is neary 3 times
 * better than before.
 */
const int MAX_NUM = 100;
int dp[1<<20][21];

class Orienteering {
public:
    Orienteering();
    void main();

private:
    struct Point{
        int x, y;
        int step;
        Point(int _x = -1, int _y = -1, int _step = 0) : x(_x), y(_y), step(_step) {}
    };

    bool ReadTerrain();
    void ComputeDistance();
    int  GetShortestPath();
    int  BFS(Point &start, Point &end);

private:
    char m_terrain[MAX_NUM][MAX_NUM];
    Point m_checkpoint[20];
    Point m_start;
    Point m_goal;

    int m_dist[20][20];
    int m_visited[MAX_NUM][MAX_NUM];
    int m_checkpoint_size;

    int m_width;
    int m_height;
    bool m_reachable;
};

Orienteering::Orienteering()
    : m_start(Point()), m_goal(Point()), m_checkpoint_size(0), m_width(0), m_height(0), m_reachable(true){
}

void Orienteering::main(){
    if(ReadTerrain()){
        ComputeDistance();
        if(!m_reachable) std::cout<<-1 <<std::endl;
        else std::cout<<GetShortestPath() <<std::endl;
    }else{
        std::cout<<-1<<std::endl;
    }
}

bool Orienteering::ReadTerrain(){
    std::cin>>m_width>>m_height;
    if(m_width < 0 || m_width > 100 || m_height < 0 || m_height > 100){
        return false;
    }

    int start_num = 0;
    int goal_num = 0;
    for(int i = 0; i < m_height; ++i){
        for(int j = 0; j < m_width; ++j){
            std::cin>>m_terrain[i][j];
            if (m_terrain[i][j] == '#' || m_terrain[i][j] == '.') {
                continue;
            } else if (m_terrain[i][j] == '@') {
                m_checkpoint[++m_checkpoint_size].x = i;
                m_checkpoint[m_checkpoint_size].y = j;
            } else if (m_terrain[i][j] == 'S') {
                m_start.x = i;
                m_start.y = j;
                start_num++;
            } else if (m_terrain[i][j] == 'G') {
                m_goal.x = i;
                m_goal.y = j;
                goal_num++;
            } else {
                return false;
            }
        }
    }

    m_checkpoint_size += 2;
    m_checkpoint[0] = m_start;
    m_checkpoint[m_checkpoint_size - 1] = m_goal;
    if(start_num != 1 || goal_num != 1 || m_checkpoint_size > 20){
        return false;
    }

    memset(m_dist, 0, sizeof(m_dist));
    return true;
}

void Orienteering::ComputeDistance(){
    for(int i = 0; i < m_checkpoint_size; ++i){
        for(int j = i + 1; j < m_checkpoint_size; ++j){
            int w = BFS(m_checkpoint[i], m_checkpoint[j]);
            if(w == INT_MAX){
                m_reachable = false;
                return ;
            }else{
                m_dist[i][j] = m_dist[j][i] = w;
            }
        }//end for j
    }//end for i
}

int Orienteering::BFS(Point &start, Point &end){
    memset(m_visited, 0, sizeof(m_visited));

    int dir_x[4] ={0, -1, 0, 1};
    int dir_y[4] ={-1, 0, 1, 0};

    std::queue<Point> que;
    que.push(start);
    m_visited[start.x][start.y] = 1;
    Point cur_point, extend;
    while(!que.empty()) {
        cur_point = que.front();
        que.pop();
        if(cur_point.x == end.x && cur_point.y == end.y){
            return cur_point.step;
        }

        for(int d = 0; d < 4; ++d){
            extend.x = cur_point.x + dir_x[d];
            extend.y = cur_point.y + dir_y[d];
            extend.step = cur_point.step + 1;

            if(extend.x >= 0 && extend.x < m_height && extend.y >= 0 && extend.y < m_width
               && m_terrain[extend.x][extend.y] != '#' && m_visited[extend.x][extend.y] == 0){
                que.push(extend);
                m_visited[extend.x][extend.y] = 1;
            }
        }//end for d
    }//end while

    return INT_MAX;
}


/**
 * Return the shortest path length between 'S' and 'G'
 *
 * Use compressed state dynamic programming to get the shortest path between 'S' and 'G'.
 * dp[s][i] mean in state s, the shortest path from Start to checkpoint i.
 * And each binary bit of s means the i-th checkpoint is visited(the bit is 1), or not visited(the bit is 0).
 * The transfer equation is: dp[s][i] = min(dp[s][k] + m_dist[k][i], dp[s][i]), m_dist is computed by BFS(the shortest
 * path length between checkpoint k and i.
 */
int Orienteering:: GetShortestPath(){
    int n = m_checkpoint_size;//include the 'S' and 'G'
    if(n == 2) return m_dist[0][1];

    for(int s = 0; s < (1 << (n - 2)); ++s) {
        for(int i = 1; i <= n - 2; ++i) {
            if(s & (1 << (i-1))) {
                if (s == (1 << (i-1))) {
                    dp[s][i] = m_dist[0][i];
                } else {
                    dp[s][i] = INT_MAX;
                    for(int j = 1; j <= n - 2; ++j) {
                        if(s & (1 << (j-1)) && i != j )
                            dp[s][i] = std::min(dp[s ^ (1<<(i-1))][j] + m_dist[j][i], dp[s][i]);
                    }
                }
            }//end if
        }//end for i
    }//end for s

    int result = dp[(1 << (n - 2)) - 1][1] + m_dist[1][n - 1];
    for(int i = 2; i < n - 1; ++i)
        result = std::min(result, dp[(1 << (n-2)) - 1][i] + m_dist[i][n - 1]);

    return result;
}

int main()
{
    Orienteering o;
    o.main();
    return 0;
}
