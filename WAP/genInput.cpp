#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <stdlib.h>
#include <unistd.h>

using namespace std;

void randPos(int w, int h, int &r, int &c){
    //srand(time(NULL));
    r = rand() % (h - 2) + 1;
    c = rand() % (w - 2) + 1;
}

void printTerrain(vector<string> &terrain){
    for(int i = 0; i < terrain.size(); ++i){
        cout<<terrain[i] <<endl;
    }
}

void writeToFile(ostream &out, vector<string> &terrain){
    for(int i = 0; i < terrain.size(); ++i){
        out<<terrain[i]<<endl;
    }
}

void generate(string &file){
    ofstream out;
    out.open(file);
    srand(time(NULL));
    int width = rand() % 100 + 1;
    int height = rand() % 100 + 1;
    out<<width<<" " <<height<<endl;
    int check_point_num = rand() % 19;
    cout<<"width: " <<width <<" height: " <<height <<" check_point_num: " <<check_point_num<<endl;

    vector<string> terrain;
    terrain.push_back(string(width, '#'));
    int r = height - 2;
    for(int i = 0; i < r; ++i){
        string line(width, '.');
        line[0] = '#';
        line[width - 1] = '#';
        terrain.push_back(line);
    }
    terrain.push_back(string(width, '#'));

    int rpos = 0, cpos = 0;
    //generate start and goal point
    randPos(width, height, rpos, cpos);
    terrain[rpos][cpos] = 'S';

    do{
        randPos(width, height, rpos, cpos);
    }while(terrain[rpos][cpos] == 'S');
    terrain[rpos][cpos] = 'G';

    //generate @
    for(int i = 0; i < check_point_num; ++i){
        do{
            randPos(width, height, rpos, cpos);
        }while(terrain[rpos][cpos] == 'S' || terrain[rpos][cpos] == 'G' || terrain[rpos][cpos] == '@');
        terrain[rpos][cpos] = '@';
    }

    //generate # in the middle of the terrain
    int tmp = (width - 1) * (height - 1) / 10;
    for(int i = 0; i < tmp; ++i){
        do{
            randPos(width, height, rpos, cpos);
        }while(terrain[rpos][cpos] == 'S' || terrain[rpos][cpos] == 'G' || terrain[rpos][cpos] == '@' || terrain[rpos][cpos] == '#');

        terrain[rpos][cpos] = '#';
    }

    printTerrain(terrain);
    writeToFile(out, terrain);

    out.close();
}

int main()
{
    int n = 100;
    for(int i = 0; i < n; ++i){
        char s[256];
        sprintf(s, "fulltest/testcase/%d", i);
        sleep(1);
        cout<<s<<endl;
        string file_name(s);
        generate(file_name);
    }
    return 0;
}
