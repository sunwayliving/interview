#include <iostream>
#include <vector>
#include <string>
#include <queue>

/**
 * @author Sun Wei
 * This implementation adopts an arraylist. In order to get better efficiency when deleting records I just
 * move the index instead of moving the array.
 * I also conducted runtime experiments to compare with the runtime of array implementation, which chose to
 * extend the array as 16,32,64,128,256...
 * The result is that the Enqueue performance of array implementation is better than arraylist implementation however
 * the arraylist implementation has significant advantages in the runtime of Dequeue.
 *
 * @param <E>
 */
class Orienteering {
public:
    Orienteering();
    void main();
    bool readTerrain();
    void computeDistance();
    int getShortestPath();

private:
    struct Point{
        int x, y;
        Point(int _x = -1, int _y = -1) : x(_x), y(_y){}
    };

    int bfs(Point &start, Point &end);

private:
    std::vector<std::string> m_terrain;
    std::vector<Point> m_checkpoint;
    std::vector<std::vector<int> > m_dist;
    Point m_start;
    Point m_goal;
    int m_width;
    int m_height;
    bool m_reachable;
};

Orienteering::Orienteering()
    : m_start(Point()), m_goal(Point()), m_width(0), m_height(0), m_reachable(true){
}

void Orienteering::main(){
    if(readTerrain()){
        computeDistance();

        if(!m_reachable){
            std::cout<<-1<<std::endl;
        }else{
            std::cout<<getShortestPath()<<std::endl;
        }
    }else{
        std::cout<<-1<<std::endl;
    }
}

bool Orienteering::readTerrain(){
    m_terrain.clear();
    std::cin>>m_width>>m_height;
    if(m_width < 0 || m_width > 100 || m_height < 0 || m_height > 100){
        return false;
    }

    std::string line;
    int start_num = 0;
    int goal_num = 0;
    for(int i = 0; i < m_height; ++i){
        std::cin>>line;
        if(line.length() != (size_t)m_width){
            return false;
        }

        m_terrain.push_back(line);
        for(size_t j = 0; j < line.length(); ++j){
            if(line[j] == 'S')                        { m_start = Point(i, j); start_num++;  }
            else if(line[j] == 'G')                   { m_goal = Point(i, j); goal_num++;    }
            else if(line[j] == '@')                   { m_checkpoint.push_back(Point(i, j)); }
            else if(line[j] == '#' || line[j] == '.') { continue; }
            else                                      { return false; }
        }
    }
    m_checkpoint.push_back(m_start);
    std::swap(m_checkpoint.back(), m_checkpoint.front());
    m_checkpoint.push_back(m_goal);

    int check_point_size = m_checkpoint.size();
    if(start_num != 1 || goal_num != 1 || check_point_size > 20){
        return false;
    }

    m_dist = std::vector<std::vector<int> >(check_point_size,
                                            std::vector<int>(check_point_size, 0));
    return true;
}

void Orienteering::computeDistance(){
    for(size_t i = 0; i < m_checkpoint.size(); ++i){
        for(size_t j = i + 1; j < m_checkpoint.size(); ++j){
            int w = bfs(m_checkpoint[i], m_checkpoint[j]);
            if(w == INT_MAX){
                m_reachable = false;
                return ;
            }

            m_dist[i][j] = m_dist[j][i] = w;
        }//end for j
    }//end for i
}

int Orienteering::bfs(Point &start, Point &end){
    std::vector<std::vector<int> > visited(m_height, std::vector<int>(m_width, 0));
    int dir[4][2] = {{0, 1}, {0, -1}, {1, 0}, {-1, 0}};

    std::queue<Point> que;
    que.push(start);
    visited[start.x][start.y] = 1;
    int step = 0;
    while(!que.empty() && visited[end.x][end.y] == 0){
        int level_size = que.size();
        step++;
        for(int i = 0; i < level_size && visited[end.x][end.y] == 0; ++i){
            Point temp = que.front();
            que.pop();

            for(int d = 0; d < 4; ++d){
                Point extend(temp.x + dir[d][0], temp.y + dir[d][1]);
                if(extend.x >= 0 && extend.x < m_height
                   && extend.y >= 0 && extend.y < m_width
                   && m_terrain[extend.x][extend.y] != '#'
                   && visited[extend.x][extend.y] == 0){
                    que.push(extend);
                    visited[extend.x][extend.y] = 1;
                }
            }//end for d
        }//end for i
    }//end while

    if(visited[end.x][end.y] == 1) return step;
    else return INT_MAX;
}

int Orienteering::getShortestPath(){
    int ans = INT_MAX;
    int n = m_checkpoint.size();
    n = n - 1;//exclude the start point
    std::vector<std::vector<int> > dp((1 << n), std::vector<int>(n + 1));
    for(int i = 0; i < (1 << n); ++i){
        for(int j = 1; j < n + 1; ++j){
            if(i == (1 << (j - 1))){
                dp[i][j] = m_dist[0][j];
            }else{
                if(i & (1 << (j - 1))){
                    dp[i][j] = INT_MAX;
                    for(int k = 1; k < n + 1; ++k){
                        if(k != j && (i & (1 << (k - 1)))){
                            dp[i][j] = std::min(dp[i][j], dp[i ^ (1 << (j - 1))][k] + m_dist[k][j]);
                        }
                    }
                }
            }//end else
        }//end for j
    }//end for i

    for(int i = 1; i < n + 1; ++i){
        ans = std::min(ans, dp[(1 << n) - 1][i] + m_dist[i][m_checkpoint.size() - 1]);
    }

    return ans;
}

int main()
{
    Orienteering o;
    o.main();
    return 0;
}
