#include <iostream>
#include <vector>

using namespace std;

struct Data{
    int m_start;
    int m_end;
    string m_str;
};

void lcs(const string &txt, const string &query, vector<Data> &res){
    if(txt.empty() || query.empty()) return ;

    int t_len = txt.length();
    int q_len = query.length();
    vector<int> tmp(t_len, 0), result(t_len, 0);
    for(int i = 0; i < q_len; ++i){
        tmp.assign(t_len, 0);
        for(int j = 0; j < t_len; ++j){
            if(query[i] == txt[j]){
                if(j == 0) tmp[j] = 1;
                else tmp[j] = result[j - 1] + 1;
            }

            if(tmp[j] >= 3){
                string sub = txt.substr(j - tmp[j] + 1, tmp[j]);
                Data d;
                d.m_start = j - tmp[j] + 1;
                d.m_end = j;
                d.m_str = sub;
                if(!res.empty() && d.m_start <= res.back().m_end){
                    res.back().m_end = max(d.m_end, res.back().m_end);
                }else{
                    res.push_back(d);
                }
            }
        }

        swap(tmp, result);
    }
}

int main()
{
    string str1, str2;
    cin>>str1 >> str2;
    vector<Data> result;
    lcs(str1, str2, result);
    for(int i = 0; i < result.size(); ++i){
        cout<<result[i].m_str<<" " << result[i].m_start<<endl;
    }
    return 0;
}
