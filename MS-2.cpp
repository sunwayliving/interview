#include <iostream>
#include <vector>
#include <string>
#include <cstdio>
#include <stdlib.h>
using namespace std;

string computeLast(string start, string end, bool &istimeok){
    int sh, sm, ss;
    int eh, em, es;

    sh = atoi(start.substr(0, 2).c_str());
    eh = atoi(end.substr(0, 2).c_str());

    sm = atoi(start.substr(3, 2).c_str());
    em = atoi(end.substr(3, 2).c_str());

    ss = atoi(start.substr(6, 2).c_str());
    es = atoi(end.substr(6, 2).c_str());

    if(sh < 0 || eh < 0 || sm < 0 || em < 0 || ss < 0 || es < 0){
        istimeok = false;
        return string("NO");
    }

    int arr1[3] = {eh, em, es};
    int arr2[3] = {sh, sm, ss};
    int last[3] = {0, 0, 0};

    for(int i = 2; i >= 0; --i){
        last[i] = last[i] + arr1[i] - arr2[i];
        if(last[i] < 0){
            if(i - 1 < 0){
                istimeok = false;
                return string("NO");
            }
            last[i] += 60;
            last[i - 1] -= 1;
        }
    }

    if(last[2] < 0 || last[1] < 0 || last[0] < 0){
        istimeok = false;
    }
    char str1[10];
    char str2[10];
    char str3[10];
    sprintf(str1, "%02d", last[0]);
    sprintf(str2, "%02d", last[1]);
    sprintf(str3, "%02d", last[2]);
    return string(str1) + ":" + string(str2) + ":" + string(str3);
}


int main()
{
    int n;
    cin>>n;
    vector<string> name(n);
    vector<string> start(n);
    vector<string> last(n);
    int idx = 0;
    for(int i = 0; i < n; ++i){
        string s1, s2, s3;
        cin>>s1>>s2>>s3;
        if(s3[0] == 'S'){
            name[idx] = s1;
            start[idx++] = s2;
        }else if(s3[0] == 'E'){
            //judge
            bool isfind = false;
            bool istimeok = true;
            for(int i = 0; i < name.size(); ++i){
                if(name[i] == s1){
                    isfind = true;
                    last[i] = computeLast(start[i], s2, istimeok);
                }
            }

            if(!isfind || !istimeok) {
                cout<<"Incorrect performance log" <<endl;
                return -1;
            }
        }else{
            cout<<"Incorrect performance log" <<endl;
            return -1;
        }
    }

    for(int i = 0; i < idx; ++i){
        cout<<name[i] << " " <<last[i] <<endl;
    }
    return 0;
}
