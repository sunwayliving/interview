#include <iostream>
#include <stack>
using namespace std;

void hanoi(const int n, const char a, const char b, const char c)
{
    if(n >= 1){
        hanoi(n-1, a, c, b);
        cout<<a << " ---> " <<c <<endl;
        hanoi(n-1, b, a, c);
    }
}

int main()
{
    char a = 'A', b = 'B', c = 'C';
    hanoi(3, a, b, c);
    return 0;
}
