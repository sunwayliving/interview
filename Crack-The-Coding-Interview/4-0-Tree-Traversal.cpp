#include <iostream>
#include <fstream>
#include <stack>
#include <queue>
#include <cmath>
using namespace std;

struct TreeNode{
    int val;
    TreeNode *lchild, *rchild;
    TreeNode(int x) : val(x), lchild(NULL), rchild(NULL){}
};

TreeNode* createTree(){
    TreeNode *T;
    int data;
    cin>>data;
    if(data == -1){
        T = NULL;
    }else{
        T = new TreeNode(data);
        T->lchild = createTree();
        T->rchild = createTree();
    }
    return T;
}

//root left right
void preOrder(TreeNode *root)
{
    if(root == NULL) return;

    stack<TreeNode*> stk;
    TreeNode *p = root;
    stk.push(p);
    while(!stk.empty()){
        p = stk.top();
        stk.pop();
        cout<<p->val <<" ";

        if(p->rchild != NULL) stk.push(p->rchild);
        if(p->lchild != NULL) stk.push(p->lchild);
    }
}

//left root right
void inOrder(TreeNode *root){
    if(root == NULL) return;
    stack<TreeNode*> stk;
    TreeNode *p = root;
    while(p || !stk.empty()){
        while(p){
            stk.push(p);
            p = p->lchild;
        }//leftmost
        p = stk.top();
        stk.pop();
        cout<<p->val <<" ";
        p = p->rchild;
    }
}

//left right root
void postOrder(TreeNode *root)
{
    if(root == NULL) return;
    stack<const TreeNode*> stk;
    const TreeNode *p, *q;
    p = root;
    do{
        while(p){
            stk.push(p);
            p = p->lchild;
        }//left most

        q = NULL;
        while(!stk.empty()){
            p = stk.top();
            stk.pop();
            //当其右孩子已被访问过或者该节点不存在有孩子，访问该节点
            if(p->rchild == q){
                cout<<p->val <<" ";
                q = p;
            }else{
                //当前节点不能访问,需要二次进栈
                stk.push(p);
                //先处理又子树
                p = p->rchild;
                break;
            }
        }
    }while(!stk.empty());
}

//http://www.cnblogs.com/AnnieKim/archive/2013/06/15/MorrisTraversal.html
//morris preOrder
void morrisPreOrder(TreeNode *root){
    if(root == NULL) return;

    TreeNode *cur;
    cur = root;
    while(cur != NULL){
        if(cur->lchild == NULL){
            cout<<cur->val<<" ";
            cur = cur->rchild;
        }else{
            //find predecessor
            TreeNode *p = cur->lchild;
            while(p->rchild != NULL && p->rchild != cur)
                p = p->rchild;
            //未线索化
            if(p->rchild == NULL){
                cout<<cur->val <<" ";
                p->rchild = cur;
                cur = cur->lchild;
            }else{//已经线索化
                p->rchild = NULL;
                cur = cur->rchild;
            }
        }
    }
}

// http://www.cnblogs.com/AnnieKim/archive/2013/06/15/MorrisTraversal.html
void morrisInOrder(TreeNode *root){
    if(root == NULL) return;

    TreeNode *cur;
    cur = root;
    while(cur != NULL){
        if(cur->lchild == NULL){//no left child exists
            cout<<cur->val <<" ";//3 lines bind together
            cur = cur->rchild;
        }else{
            //find predecessor
            TreeNode *p = cur->lchild;
            while(p->rchild != NULL && p->rchild != cur)
                p = p->rchild;
            //未线索化
            if(p->rchild == NULL){
                p->rchild = cur;
                cur = cur->lchild;
            }else{//已经线索化
                p->rchild = NULL; //cut
                cout<<cur->val <<" ";//3 lines bind together
                cur = cur->rchild;
            }
        }
    }
}

void BST(TreeNode **root, TreeNode *newNode){
    if(*root == NULL)
        *root = newNode;
    else if(newNode->val < (*root)->val)
        BST(&(*root)->lchild, newNode);
    else if(newNode->val > (*root)->val)
        BST(&(*root)->rchild, newNode);
}

TreeNode* createBST()
{
    int data;
    TreeNode *root = NULL;
    while(cin>>data){
        TreeNode *t = new TreeNode(data);
        BST(&root, t);
    }
    return root;
}

//problem 4-1
int treeDepth(TreeNode *root)
{
    if(!root) return 0;
    return max(treeDepth(root->lchild), treeDepth(root->rchild)) + 1;
}

bool isBalanced(TreeNode *root)
{
    if(!root) return true;
    int ld = treeDepth(root->lchild);
    int rd = treeDepth(root->rchild);
    if(abs(ld - rd) > 1) return false;
    else return isBalanced(root->lchild) && isBalanced(root->rchild);
}

int getGap(TreeNode *root){
    if(root == NULL) return 0;

    int max_val = INT_MIN;
    int min_val = INT_MAX;

    queue<TreeNode*> que;
    TreeNode *p =  root;
    que.push(p);
    while(!que.empty()){
        TreeNode *t = que.front();
        que.pop();
        if(t->val < min_val) min_val = t->val;
        if(t->val > max_val) max_val = t->val;

        if(t->lchild != NULL) que.push(t->lchild);
        if(t->rchild != NULL) que.push(t->rchild);
    }
    return abs(max_val - min_val);
}

int main()
{
    ifstream in;
    in.open("input.txt");
    cin.rdbuf(in.rdbuf());

    TreeNode *root = createTree();
    cout<<getGap(root) <<endl;
    in.close();
    return 0;
}
