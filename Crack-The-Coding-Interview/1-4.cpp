#include <iostream>

using namespace std;

#define ASCII_MAX 256

bool isAnagram(string s, string t)
{
    const int s_len = s.length();
    const int t_len = t.length();
    if(s_len != t_len) return false;

    int *count = (int*) malloc(sizeof(int) * ASCII_MAX);
    for(int i = 0; i < ASCII_MAX; ++i)
        count[i] = 0;
    for(int i = 0; i < s_len; ++i)
        count[s[i]]++;

    for(int j = 0; j < t_len; ++j){
        if(count[t[j]] <= 0) return false;
        else count[t[j]]--;
    }
    return true;
    free(count);
}

int main()
{
    string s1("helloworld");
    string s2("worldhello");
    cout<<isAnagram(s1, s2)<<endl;
    return 0;
}
