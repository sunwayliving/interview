#include <iostream>
#include <cstring>
using namespace std;

const int CHAR_COUNT = 10;
const int ITEM_NUM = 10000;
const int ITEM_MAX = 10;
const int MAX_NODE_COUNT = ITEM_NUM * ITEM_MAX + 1;

struct TrieNode{
    TrieNode* next[CHAR_COUNT];
    bool is_tail;
};

struct Trie{
    TrieNode *root;
    int size;

    TrieNode nodes[MAX_NODE_COUNT];
};

Trie* createTrie()
{
    Trie *T = new Trie;
    T->root = &(T->nodes[0]);
    memset(T->nodes, 0, sizeof(T->nodes));
    T->size = 1;
    return T;
}

void freeTrie(Trie *tree)
{
    delete tree;
    tree = NULL;
}

bool trieInsert(Trie *tree, const char *word)
{
    TrieNode *p = tree->root;
    while(*word){
        int curWord = *word - '0';
        if(p->next[curWord] == NULL){
            p->next[curWord] = &(tree->nodes[tree->size++]);
        }
        p = p->next[curWord];
        if(p->is_tail) return false;
        ++word;
    }
    p->is_tail = true;
    for(int i = 0; i < CHAR_COUNT; ++i){
        if(p->next[i] != NULL)
            return false;
    }
    return true;
}

void clearTrie(Trie* tree)
{
    memset(tree->nodes, 0, sizeof(tree->nodes));
    tree->size = 1;
}

int main()
{
    char word[20];
    bool isLegal = true;
    int testID = 1;
    Trie *tree = createTrie();
    while(cin>>word){
        if(word[0] == '9'){
            if(isLegal) cout<<"Set "<<testID<< " is immediately decodable"<<endl;
            else cout<<"Set "<<testID<<" is not immediately decodable"<<endl;
            ++testID;
            clearTrie(tree);
            isLegal = true;
        }else{
            if(isLegal) isLegal = trieInsert(tree, word);
        }
    }
    freeTrie(tree);
    return 0;
}
