#include <iostream>
#include <vector>
#include <stack>
#include <queue>
#include <algorithm>
using namespace std;
const int VN = 5;

void addEdge(vector<vector<int> > &edges, int start, int end);
void displayGraph(const vector<vector<int> > &edges);

void DFSCore(const vector<vector<int> > &edges, vector<bool> &visited, int start);
void DFS(const vector<vector<int> > &edges, vector<bool> &visited);

void BFS(const vector<vector<int> > &edges, vector<bool> &visited);

//************************************************************
//problem 4-2
void routeDFS(vector<vector<int> > &edges, vector<bool> &visited, int start, int end, bool &ret)
{
     visited[start] = true;

    for(int i = 0; !ret &&i < VN; ++i){
        if(edges[start][i] && !visited[i]){
            if(i == end) ret = true;
            else routeDFS(edges, visited, i, end, ret);
        }
    }
}

bool existRoute(vector<vector<int> > &edges, vector<bool> &visited, int start, int end)
{
    fill(visited.begin(), visited.end(), false);
    bool ret = false;
    for(int i = 0; i < VN; ++i){
        if(edges[start][i] && !visited[i]){
            if(i == end) return true;
            else routeDFS(edges, visited, i, end, ret);
        }
    }
    return ret;
}

//problem 4-2 using BFS
bool existRoute2(vector<vector<int> > &edges, vector<bool> &visited, int start, int end)
{
    fill(visited.begin(), visited.end(), false);
    queue<int> que;
    que.push(start);
    visited[start] = true;
    while(!que.empty()){
        int start = que.front(); que.pop();
        if(start == end) return true;

        for(int i = 0; i < VN; ++i){
            if(edges[start][i] == 1 && !visited[i]){
                visited[i] = true;
                que.push(i);
            }
        }
    }

    return false;
}


int main()
{
    //initial edges and visited array
    vector<vector<int> > edges(VN, vector<int>(VN, 0));
    vector<bool> visited(VN, false);
    cout<<"after init!"<<endl;
    displayGraph(edges);

    //create graph
    addEdge(edges, 0, 3);
    addEdge(edges, 3, 1);
    addEdge(edges, 3, 2);
    addEdge(edges, 0, 4);
    addEdge(edges, 2, 0);
    cout<<"after create:"<<endl;
    displayGraph(edges);

    cout<<existRoute(edges, visited, 2, 4)<<endl;
    //cout<<existRoute(edges, visited, 1, 2)<<endl;
    return 0;
}




//************************************************************
void displayGraph(const vector<vector<int> > &edges)
{
    for(size_t i = 0; i < edges.size(); ++i){
        for(size_t j = 0; j < edges[i].size(); ++j){
            cout<<edges[i][j] << " ";
        }
        cout<<endl;
    }
}

void addEdge(vector<vector<int> > &edges, int start, int end)
{
    edges[start][end] = 1;
}

//************************************************************
void DFSCore(const vector<vector<int> > &edges, vector<bool> &visited, int start)
{
    cout<<start <<endl;    //here is also acceptable
    visited[start] = true;

    for(int i = 0; i < VN; ++i){
        if(edges[start][i] == 1 && !visited[i])
            DFSCore(edges, visited, i);
    }

    // cout<<start <<endl;
    // visited[start] = true;
}

void DFS(const vector<vector<int> > &edges, vector<bool> &visited)
{
    fill(visited.begin(), visited.end(), false);
    for(int i = 0; i < VN; ++i){
        if(!visited[i])
            DFSCore(edges, visited, i);
    }
}

//************************************************************
//visit and then enqueue
void BFS(const vector<vector<int> > &edges, vector<bool> &visited){
    fill(visited.begin(), visited.end(), false);
    queue<int> que;
    for(int i = 0; i < VN; ++i){
        if(!visited[i]){
            cout<<i <<endl;
            visited[i] = true;
            que.push(i);

            while(!que.empty()){
                int start = que.front(); que.pop();
                for(int j = 0; j < VN; ++j){
                    if(edges[start][j] == 1 && !visited[j]){
                        cout<<j<<endl;
                        visited[j] = true;
                        que.push(j);
                    }
                }
            }
        }
    }
}
