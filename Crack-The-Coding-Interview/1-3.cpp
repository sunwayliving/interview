#include <iostream>
using namespace std;

void removeDuplicates(char s[]){
    const int len = strlen(s);
    if(len <= 1) return;

    int index = 1, i, j;
    for(i = 1; i < len; ++i){
        for(j = 0; j < index; ++j){
            if(s[j] == s[i]) break;
        }
        if(j == index) s[index++] = s[i];
    }
    s[index] = '\0';
}

int main()
{
    char s[] = "asdflkjadsf";
    removeDuplicates(s);
    cout<<s<<endl;
}
