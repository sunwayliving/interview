#include <iostream>
#include <fstream>
#include <vector>
#include <queue>
#include <stack>
using namespace std;

struct TreeNode{
    int val;
    TreeNode *lchild, *rchild, *next;
    TreeNode(int x) : val(x), lchild(NULL), rchild(NULL), next(NULL){}
};

vector<TreeNode*> populateLevel(TreeNode *root)
{
    vector<TreeNode*> result;
    queue<TreeNode*> current;
    queue<TreeNode*> nextLevel;

    if(root == NULL) return result;
    TreeNode *p = root;
    current.push(p);
    while(!current.empty()){
        //connect current level
        TreeNode *head = current.front();
        while(!current.empty()){
            p = current.front();
            current.pop();

            if(!current.empty()) p->next = current.front();
            else p->next = NULL;
            //next level
            if(p->lchild) nextLevel.push(p->lchild);
            if(p->rchild) nextLevel.push(p->rchild);
        }
        result.push_back(head);
        swap(current, nextLevel);
    }
    return result;
}

TreeNode* createTree()
{
    TreeNode *root;
    int data;
    cin>>data;
    if(data == -1){
        root = NULL;
    }else{
        root = new TreeNode(data);
        root->lchild = createTree();
        root->rchild = createTree();
    }
    return root;
}

void preOrder2(TreeNode* root){
    if(root){
        cout<<root->val<<" ";
        preOrder2(root->lchild);
        preOrder2(root->rchild);
    }
}

void preOrder(TreeNode* root){
    if(root == NULL) return;

    TreeNode *p = root;
    stack<TreeNode*> stk;
    stk.push(p);
    cout<<"in"<<endl;
    while(!stk.empty()){
        p = stk.top(); stk.pop();
        cout<<p<<endl;
        cout<<p->val<<" ";

        if(p->rchild != NULL) stk.push(p->rchild);
        if(p->lchild != NULL) stk.push(p->lchild);
    }
}

void inOrder(TreeNode* root)
{
    if (root == NULL) return;
    stack<TreeNode*> stk;
    TreeNode *p = root;
    while(p || !stk.empty()){
        while(p){
            stk.push(p);
            p = p->lchild;
        }//left most
        //visit
        p = stk.top(); stk.pop();
        cout<<p->val <<" ";
        //right child
        p = p->rchild;
    }
}

void postOrder(TreeNode *root)
{
    if(root == NULL) return;
    stack<const TreeNode*> stk;
    const TreeNode *p, *q;
    p = root;
    do{
        while(p){
            stk.push(p);
            p = p->lchild;
        }//left most
        q = NULL;
        while(!stk.empty()){
            p = stk.top(); stk.pop();
            if(p->rchild == q){
                cout<<p->val <<endl;
                q = p;
            }else{
                stk.push(p);
                p = p->rchild;
                break;
            }
        }
    }while(!stk.empty());
}

int main()
{
    fstream in;
    in.open("input.txt");
    cin.rdbuf(in.rdbuf());

    TreeNode *root = createTree();
    postOrder(root);

    // vector<TreeNode*> ret;
    // ret = populateLevel(root);
    // for(int i = 0; i < ret.size(); ++i)
    //     cout<<ret[i]->val <<endl;

    in.close();
    return 0;
}
