#include <iostream>
#include <fstream>
#include <stack>
using namespace std;

struct TreeNode{
    int val;
    TreeNode *lchild, *rchild, *parent;
    TreeNode(int x) : val(x), lchild(NULL), rchild(NULL), parent(NULL){}
};

TreeNode* createTree()
{
    TreeNode *root;
    int data;
    cin>>data;
    if(data == -1){
        root = NULL;
    }else{
        root = new TreeNode(data);

        root->lchild = createTree();
        if(root->lchild) root->lchild->parent = root;

        root->rchild = createTree();
        if(root->rchild) root->rchild->parent = root;
    }
    return root;
}

void postOrder2(TreeNode *root)
{
    if(root){
        postOrder2(root->lchild);
        postOrder2(root->rchild);
        cout<<root->val <<" ";
    }
}

void postOrder(TreeNode *root)
{
    if(root == NULL) return;
    stack<TreeNode *> stk;
    TreeNode *p = root, *q;
    do{
        while(p){
            stk.push(p);
            p = p->lchild;
        }//left most

        q = NULL;
        while(!stk.empty()){
            p = stk.top(); stk.pop();
            if(p->rchild == q){
                cout<<p->val <<" ";
                q = p;
            }else{
                stk.push(p);
                p = p->rchild;
                break;
            }
        }
    }while(!stk.empty());
}

//************************************************************
//problem 4-5
TreeNode* leftMost(TreeNode* root)
{
    if(root == NULL) return NULL;
    while(root->lchild)
        root = root->lchild;

    return root;
}

TreeNode* inorderSucc(TreeNode *node)
{
    if(node == NULL) return NULL;

    TreeNode *p;
    if(node->parent == NULL || node->rchild != NULL){
        p = leftMost(node->rchild);
    }else{//node->parent != NULL && node->rchild == NULL
        while((p = node->parent) != NULL){
            if(p->lchild == node) break; // go up till we're on left instead of right
            node = p;
        }
    }
    return p;
}

int main()
{
    ifstream in;
    in.open("input.txt");
    cin.rdbuf(in.rdbuf());

    TreeNode *root = createTree();
    TreeNode *t = inorderSucc(root->lchild);
    if(t)
        cout<<t->val<<endl;
    in.close();
    return 0;
}
