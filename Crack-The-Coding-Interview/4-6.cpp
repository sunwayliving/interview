#include <iostream>
#include <fstream>
#include <stack>
#include <map>
using namespace std;

struct TreeNode{
    int val;
    TreeNode *lchild, *rchild, *parent;
    TreeNode(int x) : val(x), lchild(NULL), rchild(NULL), parent(NULL){}
};

TreeNode* createTree()
{
    TreeNode *root;
    int data;
    cin>>data;
    if(data == -1){
        root = NULL;
    }else{
        root = new TreeNode(data);
        root->lchild = createTree();
        if(root->lchild) root->lchild->parent = root;
        root->rchild = createTree();
        if(root->rchild) root->rchild->parent = root;
    }
    return root;
}

void inOrder(TreeNode *root)
{
    if(root == NULL) return;
    stack<TreeNode*> stk;
    TreeNode *p = root;
    while(p || !stk.empty()){
        while(p){
            stk.push(p);
            p = p->lchild;
        }//left most

        p = stk.top(); stk.pop();
        cout<<p->val<<" ";

        //right
        p = p->rchild;
    }
}

void postOrder(TreeNode *root)
{
    if(root == NULL) return;
    stack<TreeNode*> stk;
    TreeNode *p = root, *q;
    do{
        while(p){
            stk.push(p);
            p = p->lchild;
        }//left most

        q = NULL;
        while(!stk.empty()){
            p = stk.top(); stk.pop();
            if(p->rchild == q){
                cout<<p->val <<" ";
                q = p;
            }else{
                stk.push(p);
                p = p->rchild;
                break;
            }
        }
    }while(!stk.empty());
}

//************************************************************
//problem 4-6, using map
TreeNode* commomAncestor(TreeNode *n1, TreeNode* n2)
{
    if(n1 == NULL || n2 == NULL) return NULL;

    map<TreeNode*, bool> pm;
    while(n1){
        pm[n1] = true;
        n1 = n1->parent;
    }

    //check
    while(n2){
        if(pm.count(n2) > 0) break;
        else n2 = n2->parent;
    }
    return n2;
}

//don't use map
TreeNode* commomAncestor2(TreeNode *n1, TreeNode *n2)
{
    if(n1 == NULL || n2 == NULL) return NULL;

    TreeNode *p, *q;
    for(p = n1; p; p = p->parent)
        for(q = n2; q; q = q->parent){
            if(p == q) return q;
        }
    return NULL;
}

//if we can't access the parent of a node
bool isFather(TreeNode* f, TreeNode *s)
{
    if(f == NULL) return false;
    else if(f == s) return true;
    else return isFather(f->lchild, s) || isFather(f->rchild, s);
}

void commomAncestor3(TreeNode *root, TreeNode *n1, TreeNode *n2, TreeNode *&ans)
{
    if(root == NULL || n1 == NULL || n2 == NULL) return;
    if(root && isFather(root, n1) && isFather(root, n2)){
        ans = root;
        commomAncestor3(root->lchild, n1, n2, ans);
        commomAncestor3(root->rchild, n1, n2, ans);
    }
}


//************************************************************

int main()
{
    ifstream in;
    in.open("input.txt");
    cin.rdbuf(in.rdbuf());

    TreeNode *root = createTree();
    postOrder(root);

    TreeNode *t = commomAncestor2(root->lchild, root->rchild);
    if(t)
        cout<<t->val<<endl;

    TreeNode *ans;
    commomAncestor3(root, root->lchild, root->rchild, ans);
    if(ans)
        cout<<ans->val <<endl;
    in.close();
    return 0;
}
