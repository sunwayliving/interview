#include <iostream>
using namespace std;

struct TreeNode{
    int val;
    TreeNode *lchild, *rchild;
    TreeNode(int x) : val(x), lchild(NULL), rchild(NULL){}
};

void InsertBST(TreeNode **root, TreeNode *newNode)
{
    if(*root == NULL) *root = newNode;
    else if(newNode->val < (*root)->val)
        InsertBST(&(*root)->lchild, newNode);
    else if(newNode->val > (*root)->val)
        InsertBST(&(*root)->rchild, newNode);
}

void createBSTFromSortedArray(TreeNode *&root, int A[], int start, int end)
{
    if(start <= end){
        int mid = (start + end) >> 1; // divided by 2
        TreeNode *T = new TreeNode(A[mid]);
        InsertBST(&root, T);
        createBSTFromSortedArray(root->lchild, A, start, mid - 1);
        createBSTFromSortedArray(root->rchild, A, mid + 1, end);
    }
}

TreeNode* sortedArrayToBST(int A[], int start, int end)
{
    TreeNode *root;
    if(start <= end){
        int mid = (start + end) >> 1;
        root = new TreeNode(A[mid]);
        root->lchild = sortedArrayToBST(A, start, mid - 1);
        root->rchild = sortedArrayToBST(A, mid + 1, end);
    }else root = NULL;
    return root;
}

void preOrder(TreeNode *root)
{
    if(root != NULL){
        cout<<root->val <<" ";
        preOrder(root->lchild);
        preOrder(root->rchild);
    }
}

int main()
{
    int A[] = {1, 2, 3, 4, 5, 6, 7, 8, 9};
    const int n = sizeof(A) / sizeof(A[0]);
    TreeNode *root = sortedArrayToBST(A, 0, n-1);
    preOrder(root);
    return 0;
}
