#include <iostream>
#include <fstream>
#include <stack>
using namespace std;

struct TreeNode{
    int val;
    TreeNode *lchild, *rchild;
    TreeNode(int x) : val(x), lchild(NULL), rchild(NULL) {}
};

TreeNode* createTree()
{
    TreeNode *root;
    int data;
    cin>>data;
    if(data == -1){
        root = NULL;
    }else{
        root = new TreeNode(data);
        root->lchild = createTree();
        root->rchild = createTree();
    }
    return root;
}

void postOrder(TreeNode *root)
{
    if (root == NULL) return;
    stack<TreeNode*> stk;
    TreeNode *p = root, *q;
    do{
        while(p){
            stk.push(p);
            p = p->lchild;
        }

        q = NULL;
        while(!stk.empty()){
            p = stk.top(); stk.pop();
            if(p->rchild == q){
                cout<<p->val <<" ";
                q = p;
            }else{
                stk.push(p);
                p = p->rchild;
                break;
            }
        }
    }while(!stk.empty());
}

//************************************************************
//problem 4-7
bool match(TreeNode *r1, TreeNode *r2)
{
    if(r1 == NULL && r2 == NULL) return true;
    else if(r1 != NULL && r2 == NULL) return true;
    else if(r1 == NULL && r2 != NULL ) return false;
    else if(r1->val != r2->val) return false;
    else return match(r1->lchild, r2->lchild) && match(r1->rchild, r2->rchild);
}

bool subTree(TreeNode *r1, TreeNode *r2)
{
    if(r1 == NULL) return false;
    else if(r1->val == r2->val){
        if(match(r1, r2)) return true; // if we don't find one, we have to continue!
    }
    return subTree(r1->lchild, r2) || subTree(r1->rchild, r2);
}

bool containTree(TreeNode *r1, TreeNode *r2){
    if(r2 == NULL) return true;
    else return subTree(r1, r2);
}

int main()
{
    ifstream in;
    in.open("input.txt");
    cin.rdbuf(in.rdbuf());
    TreeNode *root1 = createTree();
    postOrder(root1);
    in.close();

    ifstream in2;
    in2.open("input2.txt");
    cin.rdbuf(in2.rdbuf());
    TreeNode *root2 = createTree();

    postOrder(root2);
    cout<<endl;
    cout<<containTree(root1, root2)<<endl;
    in2.close();
    return 0;
}
