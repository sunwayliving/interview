#include <iostream>

using namespace std;


//************************************************************
//Implement stack
template<class ListElemType>
struct ListNode{
    ListElemType val;
    ListElemType curMin;
    ListNode *next;
    ListNode(ListElemType x) : val(x), next(NULL){}
};

template<typename ElemType>
class Stack{
public:
    ElemType top(){
        if(elems != NULL)
            return elems->val;
        else return NO_ELEM;
    }

     ElemType pop(){
         if(elems == NULL) return NO_ELEM;
         else{
             ElemType item = elems->val;
             elems = elems->next;
             return item;
         }
    }

    void push(ElemType item){
        ListNode<ElemType> *head = new ListNode<ElemType>(item);
        head->curMin = item < min ? item : min;
        head->next = elems;
        elems = head;
    }

    int stackMin(){
        if(!empty())
            return elems->curMin;
        else
            return INT_MAX;
    }

    bool empty(){
        return elems == NULL;
    }

private:
    enum ERRORCODE{
        NO_ELEM = 1,
        OUT_OF_RANGE = 2
    };

    ListNode<ElemType> *elems;
    ElemType min;
};

//************************************************************
//Implement Queue
template<typename ElemType>
class Queue{
public:
    ElemType dequeue(){
        if(head != NULL){
            ElemType item = head->val;
            head = head->next;
            return item;
        }else{
            return NO_ELEM;
        }
    }

    void enqueue(ElemType item){
        ListNode<ElemType> *t = new ListNode<ElemType>(item);
        tail->next = t;
        tail = tail->next;
    }

private:
    enum ERRORCODE{
        NO_ELEM = 1,
        OUT_OF_RANGE = 2
    };

    ListNode<ElemType> *head, *tail;
};

//************************************************************
//problem: 3-1
class Stack3{
public:
    Stack3(int size = 300){
        buf = new int[3 * size];
        ptop[0] = ptop[1] = ptop[2] = -1;
        this->stackSize = size;
    }

    ~Stack3(){
        delete[] buf;
    }

    void push(const int stackNum, const int value){
        ++ptop[stackNum];
        int index = stackNum * stackSize + ptop[stackNum];
        buf[index] = value;
    }

    int top(const int stackNum, const int value){
        int index = stackNum * stackSize + ptop[stackNum];
        return buf[index];
    }

    void pop(const int stackNum){
        --ptop[stackNum];
    }

    bool empty(const int stackNum){
        return ptop[stackNum] == -1;
    }

private:
    int *buf;
    int ptop[3];
    int stackSize;
};


//************************************************************
struct node{
    int val;
    int preIdx;
};

class Stack3_1{
public:
    Stack3_1(int totalSize = 900){
        this->totalSize = totalSize;
        ptop[0] = ptop[1] = ptop[2] = -1;
        buf = new node[totalSize];
        cur = 0;
    }

    ~Stack3_1(){
        delete buf;
    }

    void push(const int stackNum, const int value){
        buf[cur].val = value;
        buf[cur].preIdx = ptop[stackNum];
        ptop[stackNum] = cur;
        ++cur;
    }

    int top(const int stackNum){
        if(!empty(stackNum)){
            return buf[ptop[stackNum]].val;
        }else return INT_MIN;
    }

    void pop(const int stackNum){
        if(!empty(stackNum)){
            ptop[stackNum] = buf[ptop[stackNum]].preIdx;
        }
    }

    bool empty(const int stackNum){
        return ptop[stackNum] == -1;
    }

private:
    node *buf;
    int ptop[3];
    int totalSize;
    int cur;
};


int main()
{
    Stack<int> stk;
    stk.push(5);
    cout<<stk.top()<<endl;
    return 0;
}
