#include <iostream>
#include <map>
using namespace std;

struct ListNode{
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL){}
};

ListNode* createList(int n)
{
    ListNode dummy(-1);
    ListNode *p = &dummy;
    int data;
    while(n){
        data = rand() % 10;
        ListNode *t = new ListNode(data);
        p->next = t;
        p = p->next;
        --n;
    }

    return dummy.next;
}

//************************************************************
//using hashmap, using two pointers: prev and cur
ListNode* removeDup(ListNode *head)
{
    if(head == NULL || head->next == NULL) return head;

    ListNode *prev = head, *cur = prev->next;
    map<int, bool> table;
    table.insert(pair<int, bool>(head->val, true));
    while(cur != NULL){
        if(table.count(cur->val) > 0){
            prev->next = cur->next;
            ListNode *tmp = cur;
            cur = cur->next;
            delete tmp;
        }else{
            table.insert(pair<int, bool>(cur->val, true));
            prev = cur;
            cur = cur->next;
        }
    }
    return head;
}

//using hashmap, using only one pointer: cur
ListNode* removeDup2(ListNode *head)
{
    if(head == NULL || head->next == NULL) return head;

    ListNode *cur = head;
    map<int, bool> table;
    table.insert(pair<int, bool>(head->val, true));
    while(cur->next != NULL){
        if(table.count(cur->next->val) > 0){
            ListNode *tmp = cur->next;
            cur->next = cur->next->next;
            delete tmp;
        }else{
            table.insert(pair<int, bool>(cur->next->val, true));
            cur = cur->next;
        }
    }
    return head;
}

//don't use hashmap, use two pointers
ListNode* removeDup3(ListNode *head)
{
    if(head == NULL || head->next == NULL) return head;

    ListNode *prev = head, *cur = prev->next;
    ListNode *running = head;
    while(cur != NULL){
        running = head;
        while(running != cur){
            if(cur->val == running->val){
                prev->next = cur->next;
                ListNode *tmp = cur;
                cur = cur->next;
                delete tmp;
                break;
            }else{
                running = running->next;
            }
        }//end inner while
        if(running == cur){
            prev = cur;
            cur = cur->next;
        }
    }//end outer while

    return head;
}


//************************************************************
ListNode *deleteVal(ListNode *head, int val)
{
    ListNode dummy(val-1);//as long as node equal val
    dummy.next = head;
    ListNode *cur = &dummy;
    while(cur->next != NULL){
        if(cur->next->val == val){
            ListNode *tmp = cur->next;
            cur->next = cur->next->next;
            delete tmp;
        }else{
            cur = cur->next;
        }
    }
    return dummy.next;
}


//************************************************************
ListNode* findNthToLast(ListNode *head, int n)
{
    if(head == NULL || n < 1) return NULL;

    ListNode *fast = head;
    while(fast != NULL && n){
        fast = fast->next;
        --n;
    }

    if(n > 0 && fast == NULL) return NULL;

    ListNode *slow = head;
    while(fast != NULL){
        slow = slow->next;
        fast = fast->next;
    }

    return slow;
}

//************************************************************
ListNode* deleteMid(ListNode *head)
{
    if(head == NULL) return head;

    ListNode *fast = head, *slow = head;
    while(fast->next != NULL && fast->next->next != NULL){
        fast = fast->next->next;
        slow = slow->next;
    }

    //now slow points to middle
    slow->val = slow->next->val;
    slow->next = slow->next->next;
    return head;
}

//************************************************************
ListNode* addList(ListNode *head1, ListNode *head2)
{
    if(head1 == NULL) return head2;
    if(head2 == NULL) return head1;

    ListNode dummy(-1);
    ListNode *p = &dummy;
    int carry = 0, sum, digit;

    for(; head1 != NULL || head2 != NULL; head1 = (head1 == NULL) ? NULL : head1->next,
                                            head2 = (head2 == NULL) ? NULL : head2->next){
        int val1 = (head1 == NULL) ? 0 : head1->val;
        int val2 = (head2 == NULL) ? 0 : head2->val;
        sum = val1 + val2 + carry;
        carry = sum / 10;
        digit = sum % 10;
        ListNode *t = new ListNode(digit);
        p->next = t;
        p = p->next;
    }
    return dummy.next;
}

//************************************************************
//given a circular list
ListNode* findCircleBegin(ListNode *head)
{
    if(head == NULL) return head;

    ListNode *slow = head, *fast = head;
    do{
        slow = slow->next;
        fast = fast->next->next;
    }while(slow != fast);

    slow = head;
    while(slow != fast){
        slow = slow->next;
        fast = fast->next;
    }

    return slow;
}

ListNode* makeCircle(ListNode *head)
{
    ListNode *tail = head;
    while(tail->next != NULL)
        tail = tail->next;

    int n = 5;
    ListNode *tmp = head;
    while(--n)
        tmp = tmp->next;

    tail->next = tmp;
    return head;
}


void printList(ListNode *head)
{
    while(head != NULL){
        cout<<head->val<<" ";
        head = head->next;
    }
    cout<<endl;
}


int main()
{
    ListNode *head = createList(10);
    printList(head);
    makeCircle(head);
    ListNode *entrance = findCircleBegin(head);
    cout<<entrance->val<<endl;
    return 0;
}
