#include <iostream>
#include <vector>
#include <stack>
#include <queue>
#include <algorithm>
using namespace std;
const int VN = 5;

void addEdge(vector<vector<int> > &edges, int start, int end);
void displayGraph(const vector<vector<int> > &edges);

void DFSCore(const vector<vector<int> > &edges, vector<bool> &visited, int start);
void DFS(const vector<vector<int> > &edges, vector<bool> &visited);

void BFS(const vector<vector<int> > &edges, vector<bool> &visited);


int main()
{
    //initial edges and visited array
    vector<vector<int> > edges(VN, vector<int>(VN, 0));
    vector<bool> visited(VN, false);
    cout<<"after init!"<<endl;
    displayGraph(edges);

    //create graph
    addEdge(edges, 0, 3);
    addEdge(edges, 3, 1);
    addEdge(edges, 3, 2);
    addEdge(edges, 0, 4);
    cout<<"after create:"<<endl;
    displayGraph(edges);

    //DFS
    cout<<"BFS:" <<endl;
    BFS(edges, visited);
    cout<<"DFS:"<<endl;
    DFS(edges, visited);
    return 0;
}

//************************************************************
void displayGraph(const vector<vector<int> > &edges)
{
    for(size_t i = 0; i < edges.size(); ++i){
        for(size_t j = 0; j < edges[i].size(); ++j){
            cout<<edges[i][j] << " ";
        }
        cout<<endl;
    }
}

void addEdge(vector<vector<int> > &edges, int start, int end)
{
    edges[start][end] = 1;
}

//************************************************************
void DFSCore(const vector<vector<int> > &edges, vector<bool> &visited, int start)
{
    cout<<start <<endl;    //here is also acceptable
    visited[start] = true;

    for(int i = 0; i < VN; ++i){
        if(edges[start][i] == 1 && !visited[i])
            DFSCore(edges, visited, i);
    }

    // cout<<start <<endl;
    // visited[start] = true;
}

void DFS(const vector<vector<int> > &edges, vector<bool> &visited)
{
    fill(visited.begin(), visited.end(), false);
    for(int i = 0; i < VN; ++i){
        if(!visited[i])
            DFSCore(edges, visited, i);
    }
}

//************************************************************
//visit and then enqueue
void BFS(const vector<vector<int> > &edges, vector<bool> &visited){
    fill(visited.begin(), visited.end(), false);
    queue<int> que;
    for(int i = 0; i < VN; ++i){
        if(!visited[i]){
            cout<<i <<endl;
            visited[i] = true;
            que.push(i);

            while(!que.empty()){
                int start = que.front(); que.pop();
                for(int j = 0; j < VN; ++j){
                    if(edges[start][j] == 1 && !visited[j]){
                        cout<<j<<endl;
                        visited[j] = true;
                        que.push(j);
                    }
                }
            }
        }
    }
}
