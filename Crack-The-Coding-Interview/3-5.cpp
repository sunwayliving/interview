#include <iostream>
#include <stack>
using namespace std;

class MyQueue{
public:
    MyQueue(){}

    ~MyQueue(){}

    void push(int e){
        while(!sout.empty()){
            sin.push(sout.top());
            sout.pop();
        }
        sin.push(e);
    }

    int pop(){
        while(!sin.empty()){
            sout.push(sin.top());
            sin.pop();
        }
        int tmp = sout.top();
        sout.pop();
        return tmp;
    }

    int front(){
        moveStack(sin, sout);
        return sout.top();
    }

    int back(){
        moveStack(sout, sin);
        return sout.top();
    }

    int size(){
        return sin.size() + sout.size();
    }

    bool empty(){
        return sin.empty() && sout.empty();
    }

    void moveStack(stack<int> &src, stack<int> &dst){
        while(!src.empty()){
            dst.push(src.top());
            src.pop();
        }
    }

private:
    stack<int> sin, sout;
};

int main()
{

    return 0;
}
