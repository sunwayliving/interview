#include <iostream>
#include <vector>
#include <string>
#include <stack>
#include <map>

using namespace std;

map<char, int> cache;

void init(){
    cache['+'] = 0;
    cache['-'] = 0;
    cache['*'] = 1;
}

void helper(vector<int> &arr, vector<vector<int> > &result, vector<int> &path){
    if(path.size() == 4){
        result.push_back(path);
        return ;
    }

    for(int i = 0; i < arr.size(); ++i){
        auto it = find(path.begin(), path.end(), arr[i]);
        if(it == path.end()){
            path.push_back(arr[i]);
            helper(arr, result, path);
            path.pop_back();
        }
    }
}

int getMaxExpressionValue(vector<int> arr){
    if(arr.empty()) return 0;

    vector<vector<int> > all_opnd;
    vector<int> path;
    //get all permutations
    helper(arr, all_opnd, path);

    string all_op[6] = {"+-*", "+*-", "-+*", "-*+", "*+-", "*-+"};
    int max_res = -100000000;
    for(int i = 0; i < (int)all_opnd.size(); ++i){
        for(int j = 0; j < 6; ++j){
            //initial all operands
            stack<int> stk;
            for(int p = 0; p < 4; ++p){
                stk.push(all_opnd[i][p]);
            }
            //use each operator to compute
            for(int q = 0; q < 3; ++q){
                char ch = all_op[j][q];
                int s = stk.top(); stk.pop();
                int f = stk.top(); stk.pop();
                switch(ch){
                case '+':
                    stk.push(f + s);
                    break;
                case '-':
                    stk.push(f - s);
                    break;
                case '*':
                    stk.push(f * s);
                    break;
                default:
                    break;
                }
            }
            //compare result
            max_res = max(max_res, stk.top());
        }
    }
    return max_res;
}

int main()
{
    int a[] = {-1, 2, 3, 0};
    int n = sizeof(a) / sizeof(*a);
    vector<int> arr(a, a + n);
    cout<<getMaxExpressionValue(arr) <<endl;
    return 0;
}
