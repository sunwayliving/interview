#include <iostream>
#include <cstdio>
#include <vector>
#include <string>
#include <algorithm>

using namespace std;

void helper(vector<string> &deck, vector<vector<string> > &res, vector<string> &path){
    if(path.size() == 3){
        res.push_back(path);
        return;
    }

    for(int i = 0; i < deck.size(); ++i){
        vector<string>::iterator it = find(path.begin(), path.end(), deck[i]);
        if(it == path.end()){
            path.push_back(deck[i]);
            helper(deck, res, path);
            path.pop_back();
        }
    }
}

vector<vector<string> > getAllDeck(vector<string> &deck){
    vector<vector<string> > res;
    if(deck.empty()) return res;

    vector<string> path;
    helper(deck, res, path);
    return res;
}

int main()
{
    vector<string> deck;
    deck.push_back("红桃A");
    deck.push_back("黑桃Q");
    deck.push_back("方块J");
    deck.push_back("草花10");
    vector<vector<string> > res;
    res = getAllDeck(deck);
    cout<<res.size() <<endl;
    for(int i = 0; i < res.size(); ++i){
        for(int j = 0; j < res[i].size(); ++j){
            cout<<res[i][j] <<" ";
        }
        cout<<endl;
    }
    return 0;
}
