#include <iostream>
#include <string>
#include <vector>

using namespace std;

vector<string> equip;
vector<float> prob;

void init(){
    equip.push_back("A");
    prob.push_back(0.3);

    equip.push_back("B");
    prob.push_back(0.2);

    equip.push_back("C");
    prob.push_back(0.5);

    for(int i = 1; i < (int)prob.size(); ++i){
        prob[i] = prob[i - 1] + prob[i];
    }
}

string drop(){
    if(equip.empty())
        return string("No");

    //srand(time(NULL));
    float p = rand() * 1.0 / RAND_MAX;
    cout<<p <<endl;
    string res = equip[0];
    int i;
    for(i = 0; i < (int)prob.size(); ++i){
        if(p < prob[i])
            break;
    }
    res = equip[i];
    return res;
}

int main()
{
    init();
    cout<<drop() <<endl;
}
