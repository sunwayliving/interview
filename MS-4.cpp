#include <iostream>
#include <cstdio>
#include <algorithm>

using namespace std;

void cmd1(string &str, int s, int e, char c){
    s--, e--;
    for(int i = s; i <= e; ++i){
        str[i] = c;
    }
}

void cmd2(string &str, int s, int e, int t){
    s--, e--;
    t %= 26;
    for(int i = s; i <= e; ++i){
        if(str[i] + t > 'Z'){
            str[i] = 'A' + (t - ('Z' - str[i]) - 1);
        }else{
            str[i] = str[i] + t;
        }
    }
}

void cmd3(string &str, int k){
    std::rotate(str.begin(), str.begin() + k, str.end());
}

void cmd4(string &str, int i, int j){
    if(i > j) return;

    cmd4(str, i + 1, j);
    cmd2(str, i, j, 1);
}

int main()
{
    int n, m;
    string str;
    cin>>n>>m;
    cin>>str;
    cout<<str<<endl;
    char cmd[4];
    int c0, a1, a2, a3;
    char c4;
    for(int i = 0; i < m; ++i){
        scanf("%s %d", cmd, &c0);
        if(c0 == 1){
            scanf("%d %d %c", &a1, &a2, &c4);
            cout<<a1<<" " <<a2 <<" " <<c4 <<endl;
            cmd1(str, a1, a2, c4);
            cout<<str<<endl;
        }else if( c0 == 2){
            scanf("%d %d %d", &a1, &a2, &a3);
            cout<<a1 <<" " <<a2 <<" " <<a3 <<endl;
            cmd2(str, a1, a2, a3);
            cout<<str<<endl;
        }else if(c0 == 3){
            scanf("%d", &a1);
            cout<<a1 <<endl;
            cmd3(str, a1);
            cout<<str<<endl;
        }else if(c0 == 4){
            scanf("%d %d", &a1, &a2);
            cout<<a1 <<" " <<a2 <<endl;
            cmd4(str, a1, a2);
            cout<<str<<endl;
        }
    }
    cout<<str<<endl;
    return 0;
}
