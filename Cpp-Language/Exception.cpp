#include <iostream>
#include <stdexcept>
#include <cstring>
#include <cmath>
using namespace std;

//************************************************************
//异常抛出过程中，栈反解的时候又抛出异常导致程序强制异常退出
void terminatro(){
    cout<<"I'll be back."<<endl;
    exit(0);
}

//void (*old_t)() = set_terminate(terminator);

class Botch{
public:
    class Fruit{};
    void f(){
        cout<<"Botch::f()"<<endl;
        throw Fruit();
    }

    ~Botch(){ throw 'c'; }
};


//************************************************************
//使用 stdexcept
class MyError : public runtime_error{
public:
    MyError(const string& msg = "") : runtime_error(msg){}
};

//************************************************************
//异常规格说明的继承
class MyExceptBase{
public:
    void foo() throw(){

    }
};

class MyExceptDerived : public MyExceptBase{
public:
    void foo() throw(MyError){
        cout<<"derived" <<endl;
    }
};



int main()
{
    try{
        return 0;
    }finally{
        cout<<"finally"<<endl;
    }

}
