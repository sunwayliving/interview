#include <iostream>
#include <cstdio>
using namespace std;

class T{
public:
    T(int a){
        cout<<a <<endl;
    }
};

int main()
{
    //T array[10];
    //T *array2 = new T[10];
    T array[2] = { T(1), T(2) };

    cout<<array[0]<<endl;
    return 0;
}
