#include <iostream>
#include <cstdio>
#include <fstream>
#include <exception>
using namespace std;

class  Base{

public:
    virtual void say(){
        cout<<"I'm base"<<endl;
    }

public:
    Base(int a = 1){
        say();
    }
};


class Derived : public Base{
public:
    virtual void say(){
        cout<<"I'm derived"<<endl;
    }

    Derived(){
        say();
    }
};

//************************************************************
//对象切片问题
class SliceBase{
public:
    void get(){}
    void set(){}
public:
    int b;
};

class SliceDerived : public SliceBase{
public:
    void print(){}
    void getB(){}
};


//************************************************************
//异常机制练习
class MyError{

public:
    MyError(const char* const msg = 0) : data(msg){}
    const char* const data; //
    void say(){
        cout<<data<<endl;
    }
};

class Person{
public:
    const char* const name;
    Person(const char* const _name) : name(_name){}
    void sayName(){
        try{
            throw MyError("sunwei throw a exception");
        }catch(MyError &me){
            me.say();
        }
    }
};

void f(){
    throw MyError("Something bad hap pended");
}

//************************************************************
class NewClass{
public:
    NewClass(){
        cout<<"NewClass Cons" <<endl;
    }

    ~NewClass(){
        cout<<"NewClass DeCons" <<endl;
    }
    void* operator new(size_t, void *pMem) throw(std::bad_alloc);
    void* operator new[](size_t) throw(std::bad_alloc);

    void operator delete(void *p, size_t size) throw();
    void operator delete[](void *p, size_t size) throw();

    void say(){
        cout<<"I'm new class" <<endl;
    }
    int a;
};

void* NewClass::operator new(size_t size, void *pMem) throw(std::bad_alloc){
    cout<<"new" <<endl;
    cout<<size<<endl;
    return ::operator new(size, pMem);
}

void* NewClass::operator new[](size_t size) throw(std::bad_alloc){
    cout<<"new[]" <<endl;
    cout<<size<<endl;
    return ::operator new(size);
}

void NewClass::operator delete(void *p, size_t size) throw(){
    cout<<"delete" <<endl;
    cout<<size <<endl;
}

void NewClass::operator delete[](void *p, size_t size) throw(){
    cout<<"delete[]" <<endl;
    cout<<size<<endl;
}

class NC2{
public:
    NC2(){
        cout<<"NC2" <<endl;
    }
};

static NewClass nc;
static NC2 nc2;

int main()
{
    char *str = new char[1];
    str = "abcdef";
    printf("%s\n", str);
    return 0;
}

/*char *pMem = new char[1000];
  printf("%0x\n", pMem);
  NewClass *nc = new (pMem + 2)NewClass;
  printf("%0x\n", nc);
  cout<<nc<<endl;
  NewClass *nc_array = new NewClass[10];
  delete nc;
  delete[] nc_array;
  int *b = NULL;
  delete b;*/
