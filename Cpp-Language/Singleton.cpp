#include <iostream>
using namespace std;

//懒汉单例
class SingletonLazy{
private:
    static SingletonLazy *instance;
    SingletonLazy(){
        cout<<"Lazy Singleton"<<endl;
    } //禁止随便从外部new实例

public:
    static SingletonLazy* getInstance1();
};

SingletonLazy* SingletonLazy::instance = NULL; //必须有，否则只有声明，没有定义，报链接错误！
SingletonLazy* SingletonLazy::getInstance1(){
    if(nullptr == instance){
        instance = new SingletonLazy();
    }
    return instance;
}

//懒汉单例
class SingletonHunger{
private:
    SingletonHunger(){
        cout<<"Hungry Singleton"<<endl;
    }
public:
    static SingletonHunger* getInstance(){
        //局部静态方法
        static SingletonHunger *instance = new SingletonHunger();
        return instance;
    }
};

int main()
{
    return 0;
}
