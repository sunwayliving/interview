#include <iostream>
using namespace std;

int main()
{
    const int val = 1024;
    //int &ref2 = val; //wrong nonconst reference to a const object, compile error
    int i = 43;
    const int &r = 42;
    const int &r2 = r + 2;

    int j = 52;
    //int &refval = j + 2;
    //int &refval2 = 10; //initialize must be a object
    int &refval2 = j;
    &refval2 = i;
    return 0;
}
