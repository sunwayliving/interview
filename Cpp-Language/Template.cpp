#include <iostream>
#include <cstdio>
using namespace std;

//************************************************************
//typename 明确标识符为类型
template<class T>
class X{
   typename T::id i;
public:
    void f(){i.g();}
};

class Y{
public:
    class id{
    public:
        void g(){}
    };
};

//************************************************************
//模板返回类型优先声明
template<typename P, typename R>
R implicit_cast(const P& p){
    return p;
}

//************************************************************
//选择匹配模板中特化程度最高的模板
template<typename U, typename T>
void f(U){
    cout<<"T"<<endl;
}

template<typename T>
void f(T){
    cout<<"T*"<<endl;
}

template<typename T>
void f(const T*){
    cout<<"const T*"<<endl;
}

int main()
{
    f<int, int>(1);
    return 0;
}
