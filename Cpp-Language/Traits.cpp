#include <iostream>
using namespace std;

template<class T>
T const& max(T const &a, typename iterator_traits<T*>::value_type const &b)
{
    typedef T S;
    return a > b ? a : b;
}

int main()
{
    double m = max(1.0, 2);
    cout<<m<<endl;
    return 0;
}
