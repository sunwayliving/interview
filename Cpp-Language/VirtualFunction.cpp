#include <iostream>
#include <string>
using namespace std;

enum note{middleC, Cshare, Eflat};

class Instrument{
public:
    virtual void play(note) const = 0;
};

void Instrument::play(note) const{
    cout<<"instrument play "<<endl;
}

class Wind : public Instrument{
public:
    void play(note) const{
        cout<<"wind play"<<endl;
    }
};

void tune(Instrument &i){
    i.play(middleC);
}

//************************************************************
class NoVirtual{
    //int a;
public:
    void x() const{}
    int i() const{ return 1;}
};

class OneVirtual{
    //int a;
public:
    virtual void x() const{}
    int i() const {return 1;}
};

class TwoVirtuals{
    //int a;
public:
    virtual void x() const{}
    virtual int i() const {return 1;}
};

//************************************************************
class Person{
public:
    int age;
    string name;
    Person(){} //without this, it will wrong
    Person(int a) : age(a){}
};

class Student : public Person{
public:
    int id;
    Student(int idt) : id(idt){}
};

//************************************************************
class Base1{
public:
    int a;
    ~Base1() { cout<<"~Base1()" <<endl; }
};

class Derived1 : public Base1{
public:
    virtual void say(){
        cout<<"derived say" <<endl;
    }
    ~Derived1() { cout<<"~Derived1()" <<endl; }
};

class Base2{
public:
    virtual ~Base2() { cout<<"~Base2()" <<endl; }
};

class Derived2 : public Base2{
public:
    ~Derived2() { cout<<"~Derived2()" <<endl; }
};

typedef void (*PMF)();

//************************************************************
class M{
public:
    ~M(){
        cout<<"decon of M" <<endl;
    }
    const int operator*(M o){
        return val * o.val;
    }
    int val;
};


class TextBlock{
public:
    TextBlock(string str){
        m_text = str;
    }

    void setLength(int x) const;

    const char& operator[](size_t pos) const{
        return m_text[pos];
    }

    char& operator[](size_t pos){
        return const_cast<char&>(
                static_cast<const TextBlock&>(*this)[pos]);
    }
private:
    mutable int length;
    string m_text;
    M *m_m;
};

const int add(int a, int b){
    return a + b;
}

int main()
{
    int a = 2, b = 1;
    int c = add(a, b);
    c = 5;
    cout<<c <<endl;
    return 0;
}
