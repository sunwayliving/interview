#include <iostream>
#include <cmath>
#include <cstring>
using namespace std;

void printNum(const char *str){
    const char *p = str;
    while(*p =='0')
        p++;
    if(*p == '\0') printf("0\n");
    else printf("%s\n", p);
}

void allNum(char *str, int start, int end){
    //退出条件
    if(start >= end)
        printNum(str);

    int i, j;
    for(i = start; i < end; ++i){
        for(j = 0; j <= 9; ++j){
            str[i] = j + '0';
            allNum(str, i + 1, end);
        }
    }
}

int main()
{
    int n;
    cin>>n;

    char *strNum = (char*) malloc(sizeof(char) * n + 1);
    memset(strNum, '0', n);
    strNum[n] = '\0';
    allNum(strNum, 0, n);
    free(strNum);
    return 0;
    /*char *strnum = (char*) malloc(sizeof(char) * n + 1);
    memset(strnum, '0', n);
    strnum[n] = '\0';

    int cnt = pow(10, n) - 1;
    cout<<0<<endl;
    while(cnt--){
        int carry = 1;
        for(int i = n - 1; i >= 0; --i){
            int tmp = carry + strnum[i] - '0';//attention1 '0'
            carry = tmp / 10;
            strnum[i] = tmp % 10 + '0';//attention2 '0'
        }
        printNum(strnum);
    }
    free(strnum);*/
    return 0;
}
