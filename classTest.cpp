#include <iostream>
#include <cstdio>
using namespace std;

class ClassA{
public:
    virtual ~ClassA(){ cout<< "deconstructor of A"; };
    virtual void FunctionA(){};
};

class ClassB{
public:
    virtual void FunctionB(){};
};


class Base{
public:
    int m, n;
    virtual int add(int a, int b){
        cout<<"here" <<endl;
        return a + b;
    }

    virtual int minus(int a, int b){
        cout<<"there"<<endl;
        return a - b;
    }
    ~Base(){
        cout<<"Base deconstructor called" <<endl;
    }
};

class ClassC : public ClassA, public ClassB{
public:

};

void test(){
    ClassA a;
    cout<<" a" <<endl;
}


typedef int (*PMF)(int, int);
int (*mypf)(int , int);

int main()
{
    Base b;
    b.m = 12;
    int offset = 1;
    unsigned long *vtbl = (unsigned long*)(*(unsigned long*)&b) + offset;
    PMF myp = (PMF) *vtbl;
    cout<<(*myp)(5, 3) <<endl;

    cout<<"data"<<endl;
    int *tmp = (int*)((unsigned long*)&b + 1);
    cout<<*tmp<<endl;

    int a = 1;
    cout<<typeid(a).name() <<endl;
    cout<<typeid(b).name() <<endl;
    cout<<b.minus(3, 5) <<endl;
    cout<<md5(a)<<endl;
    return 0;
}
