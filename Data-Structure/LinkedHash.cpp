#include <iostream>
using namespace std;

struct ListNode{
    int key;
    ListNode *next;
    ListNode(int x = 0) : key(x), next(NULL){}
};

class HashTable{
public:
    HashTable(int length = 0);
    ~HashTable();

    int getHashAddr(int key);

    bool insertHashTable(int key);
    bool searchHashTable(int key, int *addr);
private:
    ListNode* (*m_elems);
    int m_length;
};

HashTable::HashTable(int length){
    m_length = length;
    m_elems = new ListNode*[length];
    for(int i = 0; i < m_length; ++i){
        m_elems[i] = new ListNode;
    }
}

HashTable::~HashTable(){
    //delete the m_elems;
}

int HashTable::getHashAddr(int key){
    return key % m_length;
}

bool HashTable::insertHashTable(int key){
    int addr = getHashAddr(key);
    ListNode *t = new ListNode(key);
    t->next = m_elems[addr]->next;
    m_elems[addr]->next = t;
    return true;
}

bool HashTable::searchHashTable(int key, int *addr){
    *addr = getHashAddr(key);
    ListNode *dummy = m_elems[*addr];
    ListNode *p = dummy->next;
    while(p != NULL){
        if(p->key == key)
            return true;

        p = p->next;
    }
    return false;
}

int main()
{
    int a[12] = {12, 67, 56, 16, 25, 37, 22, 29, 15, 47, 48, 34};
    HashTable map(50);
    for(int i = 0; i < 12; ++i){
        map.insertHashTable(a[i]);
    }
    int addr;
    cout<<map.searchHashTable(56, &addr)<<endl;
    cout<<map.searchHashTable(25, &addr)<<endl;
    cout<<map.searchHashTable(1, &addr)<<endl;
}
