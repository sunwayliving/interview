#include <iostream>
#include <cstring>
using namespace std;

struct Heap{
    int *elems;
    int size;
};

Heap* createHeap(int cap){
    Heap *h = (Heap*)malloc(sizeof(Heap));
    h->size = cap;
    h->elems = (int*) malloc(sizeof(int) * cap);
    return h;
}

void heapDestroy(Heap *h){
    free(h->elems);
    free(h);
}

void heap_sift_down(const Heap *h, const int start){
    int i = start;
    int tmp = h->elems[start];
    for(int j = 2*i + 1; j < h->size; j = 2*i + 1){
        if(j + 1 < h->size){
            if(h->elems[j + 1] < h->elems[j])
                j = j + 1;
        }

        if(tmp < h->elems[j]){
            break;
        }else{
            h->elems[i] = h->elems[j];
            i = j;
        }
    }
    h->elems[i] = tmp;
}

void heap_sort(int a[], int n){
    Heap *h;
    h = createHeap(n);
    int *old_mem = h->elems;
    h->elems = a;

    int i = (h->size - 2) / 2;
    while(i >= 0){
        heap_sift_down(h, i);
        --i;
    }

    for(int i = h->size - 1; i > 0; --i){
        swap(h->elems[i], h->elems[0]);
        h->size--;
        heap_sift_down(h, 0);
    }

    h->elems = old_mem;
    heapDestroy(h);
}

int main()
{
    int a[] = {2, 0, 3, 4, 5, 6, 1, 9, 7, 8};
    heap_sort(a, 10);
    for(int i = 0; i < 10; ++i)
        cout<<a[i]<<" ";
    cout<<endl;
    return 0;
}
