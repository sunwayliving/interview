#include <iostream>
using namespace std;

struct ListNode{
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL){}
};

ListNode* createList(int n){
    ListNode dummy(-1);
    ListNode *p = &dummy;
    for(int i = 0; i < n; ++i){
        ListNode *t = new ListNode(i);
        p->next = t;
        p = p->next;
    }
    return dummy.next;
}

ListNode* reverse(ListNode *head){
    ListNode *prev = NULL;
    ListNode *cur = head;
    ListNode *nxt = NULL;
    while(cur != NULL){
        nxt = cur->next;//save
        cur->next = prev;//change
        prev = cur;
        cur = nxt;//move
    }
    head = prev; //cur == NULL
    return head;
}

ListNode* reverseRecur(ListNode *head){
    if(head == NULL || head->next == NULL){
        return head;
    }else{
        ListNode *newHead = reverseRecur(head->next);
        head->next->next = head;
        head->next = NULL;
        return newHead;
    }
}

void printList(ListNode* head){
    ListNode *p = head;
    while(p != NULL){
        cout<<p->val <<" ";
        p = p->next;
    }
    cout<<endl;
}

int main()
{
    ListNode *head = createList(10);
    printList(head);
    ListNode *rhead = reverseRecur(head);
    printList(rhead);
    return 0;
}
