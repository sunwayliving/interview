#include <iostream>
#include <stack>
#include <queue>
#include <algorithm>
#include <vector>
using namespace std;
const int VN = 5;

void addEdge(vector<vector<int> > &edges, int start, int end){
    edges[start][end] = 1;
    edges[end][start] = 1;
}

void displayGraph(const vector<vector<int> > &edges)
{
    for(size_t i = 0; i < edges.size(); ++i){
        for(size_t j = 0; j < edges[i].size(); ++j){
            cout<<edges[i][j] << " ";
        }
        cout<<endl;
    }
}

void dfscore(vector<vector<int> > &edges, vector<bool> &visited, int start){
    visited[start] = true;
    cout<<start<<" ";

    for(int i = 0; i < edges[start].size(); ++i){
        if(edges[start][i] == 1 && !visited[i])
            dfscore(edges, visited, i);
    }
}

void dfs(vector<vector<int> > &edges, vector<bool> &visited){
    fill(visited.begin(), visited.end(), false);
    for(int i = 0; i < edges.size(); ++i){
        if(!visited[i]){
            dfscore(edges, visited, i);
        }
    }
}

void dfs2(vector<vector<int> > &edges, vector<bool> &visited){
    fill(visited.begin(), visited.end(), false);

    stack<int> stk;
    for(int i = 0; i < edges.size(); ++i){
        if(!visited[i]){
            visited[i] = true;
            stk.push(i);
            while(!stk.empty()){
                int t = stk.top();
                stk.pop();
                cout<<t<<" ";//the only difference

                for(int j = 0; j < edges[t].size(); ++j){
                    if(edges[t][j] == 1 && !visited[j]){
                        visited[j] = true;
                        stk.push(j);
                    }
                }//end for
            }//end while
        }//end if
    }//end for
}

void bfs(vector<vector<int> > &edges, vector<bool> &visited){
    fill(visited.begin(), visited.end(), false);

    queue<int> que;
    for(int i = 0; i < edges.size(); ++i){
        if(!visited[i]){
            visited[i] = true;
            que.push(i);
            while(!que.empty()){
                int t = que.front();
                que.pop();
                cout<<t <<" ";

                for(int j = 0; j < edges[t].size(); ++j){
                    if(edges[t][j] == 1 && !visited[j]){
                        visited[j] = true;
                        que.push(j);
                    }
                }//end for
            }//end while
        }// end if
    }//end for
}


//总结而言，所有所搜算法，在节点进入数据结构的时候就应该标志位 已经访问，这样在出现环的时候不会出现重复访问；
int main()
{
    //initial edges and visited array
    vector<vector<int> > edges(VN, vector<int>(VN, 0));
    vector<bool> visited(VN, false);
    cout<<"after init!"<<endl;
    displayGraph(edges);

    //create graph
    addEdge(edges, 0, 3);
    addEdge(edges, 3, 1);
    addEdge(edges, 3, 2);
    addEdge(edges, 0, 4);
    addEdge(edges, 4, 1);
    cout<<"after create:"<<endl;
    displayGraph(edges);

    //DFS
    cout<<"BFS:" <<endl;
    bfs(edges, visited);
    cout<<endl;
    cout<<"DFS:"<<endl;
    dfs2(edges, visited);
    return 0;
}
