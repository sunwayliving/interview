#include <iostream>
#include <fstream>
#include <stack>
#include <queue>
using namespace std;

struct TreeNode{
    int val;
    TreeNode *left, *right;
    TreeNode(int x) : val(x), left(NULL), right(NULL){}
};

TreeNode* createTree(){
    TreeNode *T;
    int data;
    cin>>data;
    if(data == -1){
        T = NULL;
    }else{
        T = new TreeNode(data);
        T->left = createTree();
        T->right = createTree();
    }
    return T;
}

//1 while
void preOrder(TreeNode *root){
    if(root == NULL) return;

    stack<TreeNode*> stk;
    stk.push(root);
    while(!stk.empty()){
        TreeNode *t = stk.top();
        stk.pop();
        cout<<t->val<<" ";

        if(t->right != NULL) stk.push(t->right);
        if(t->left != NULL) stk.push(t->left);
    }
}

//2 while
void inOrder(TreeNode *root){
    if(root == NULL) return;

    stack<TreeNode*> stk;
    TreeNode *p = root;
    while(!stk.empty() || p != NULL){
        while(p != NULL){
            stk.push(p);
            p = p->left;
        }

        p = stk.top();
        stk.pop();
        cout<<p->val<<" ";

        p = p->right;
    }
}

//1 do 3 while
void postOrder(TreeNode *root){
    if(root == NULL) return;

    stack<TreeNode*> stk;
    TreeNode *p = root, *q = NULL;
    do{
        while(p){
            stk.push(p);
            p = p->left;
        }

        q = NULL;
        while(!stk.empty()){
            p = stk.top();
            stk.pop();
            //当其右孩子已被访问过或者该节点不存在有孩子， 访问该节点
            if(q == p->right){
                cout<<p->val <<" ";
                q = p;
            }else{
                //当前节点不能访问，二次进栈
                stk.push(p);
                p = p->right;
                break;
            }
        }
    }while(!stk.empty());
}

void dfs(TreeNode *root){
    if(root == NULL) return;

    stack<TreeNode*> stk;
    stk.push(root);
    TreeNode *p;
    while(!stk.empty()){
        p = stk.top();
        stk.pop();
        cout<<p->val<<" ";

        if(p->right != NULL) stk.push(p->right);
        if(p->left != NULL) stk.push(p->left);
    }
}

void bfs(TreeNode *root){
    if(root == NULL) return;

    queue<TreeNode*> que;
    TreeNode *p = root;
    que.push(p);
    while(!que.empty()){
        p = que.front();
        que.pop();
        cout<<p->val <<" ";

        if(p->left != NULL) que.push(p->left);
        if(p->right != NULL) que.push(p->right);
    }
}

int main()
{
    ifstream in;
    in.open("input.txt");
    cin.rdbuf(in.rdbuf());

    TreeNode *root = createTree();
    dfs(root);
    cout<<endl;
    bfs(root);
    in.close();
    return 0;
}
