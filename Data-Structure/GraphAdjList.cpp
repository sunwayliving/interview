#include <iostream>
#include <vector>
#include <stack>
#include <queue>
#include <algorithm>
using namespace std;
const int VN = 5;

struct ListNode{
    int val;
    ListNode *next;
    ListNode(int x) : val(x), next(NULL){}
};

//undirected graph
void addEdge(vector<ListNode*> &edges, int start, int end){
    ListNode *dummy = edges[start];
    ListNode *t = new ListNode(end);
    t->next = dummy->next;
    dummy->next = t;


    dummy = edges[end];
    t = new ListNode(start);
    t->next = dummy->next;
    dummy->next = t;
}

void dfs(vector<ListNode*> &edges, vector<bool> &visited){
    fill(visited.begin(), visited.end(), false);
    stack<int> stk;
    for(int i = 0; i < edges.size(); ++i){
        if(!visited[i]){
            stk.push(i);
            visited[i] = true;
            while(!stk.empty()){
                int t = stk.top();
                stk.pop();
                cout<<t<<" ";
                visited[t] = true;

                ListNode *h = edges[t]->next;
                while(h != NULL){
                    if(!visited[h->val]){
                        stk.push(h->val);
                        visited[h->val] = true;
                    }
                    h = h->next;
                }//end while
            }//end while
        }//end if
    }//end for
}

void bfs(vector<ListNode*> &edges, vector<bool> &visited){
    fill(visited.begin(), visited.end(),false);

    queue<int> que;
    for(int i = 0; i < edges.size(); ++i){
        if(!visited[i]){
            visited[i] = true;
            que.push(i);

            while(!que.empty()){
                int t = que.front();
                que.pop();
                cout<<t <<" ";

                ListNode *h = edges[t]->next;
                while(h != NULL){
                    if(!visited[h->val]){
                        que.push(h->val);
                        visited[h->val] = true;
                    }
                    h = h->next;
                }
            }
        }
    }
}

int main()
{
    vector<ListNode*> edges(VN, NULL);
    for(int i = 0; i < VN; ++i){
        edges[i] = new ListNode(-1);
    }

    vector<bool> visited(VN, false);
    addEdge(edges, 0, 3);
    addEdge(edges, 3, 1);
    addEdge(edges, 3, 2);
    addEdge(edges, 0, 4);
    cout<<"dfs: ";
    dfs(edges, visited);
    cout<<"bfs: ";
    bfs(edges, visited);
    cout<<endl;

    return 0;
}
