#include <iostream>
using namespace std;

#define NULLKEY -32768

class HashTable{
public:
    HashTable(int length);
    ~HashTable();

    int getHashAddr(int key);
    //linear way to resolve conflict
    bool insertHashTableLinear(int key);
    bool searchHashTableLinear(int key, int *addr);
    //random way to resolve conflict
    bool insertHashTableRand(int key);
    bool searchHashTableRand(int key, int *addr);
private:
    int *m_elems;
    int m_length;
};

HashTable::HashTable(int length){
    m_length = length;
    m_elems = new int[length];
    for(int i = 0; i < m_length; ++i){
        m_elems[i] = NULLKEY;
    }
}

HashTable::~HashTable(){
    delete m_elems;
}

int HashTable::getHashAddr(int key){
    return key % m_length;
}

//************************************************************
//linear way to resolve conflict
bool HashTable::insertHashTableLinear(int key){
    int addr = getHashAddr(key);
    while(m_elems[addr] != NULLKEY){
        addr = (addr + 1) % m_length;
    }
    m_elems[addr] = key;
    return true;
}

bool HashTable::searchHashTableLinear(int key, int *addr){
    *addr = getHashAddr(key);
    while(m_elems[*addr] != key){
        *addr = (*addr + 1) % m_length;
        if(*addr == getHashAddr(key) || m_elems[*addr] == NULLKEY)
            return false;
    }
    return true;
}

//************************************************************
//random way to resolve conflict
bool HashTable::insertHashTableRand(int key){
    int addr = getHashAddr(key);
    while(m_elems[addr] != NULLKEY){
        addr = (addr + rand()) % m_length;
    }
    m_elems[addr] = key;
    return true;
}

bool HashTable::searchHashTableRand(int key, int *addr){
    *addr = getHashAddr(key);
    while(m_elems[*addr] != key){
        *addr = (*addr + rand()) % m_length;
        if(m_elems[*addr] == NULLKEY || *addr == getHashAddr(key))
            return false;
    }
    return true;
}

int main()
{
    int a[12] = {12, 67, 56, 16, 25, 37, 22, 29, 15, 47, 48, 34};
    HashTable map(50);
    for(int i = 0; i < 12; ++i){
        map.insertHashTableRand(a[i]);
    }
    int addr;
    cout<<map.searchHashTableRand(56, &addr)<<endl;
    cout<<map.searchHashTableRand(25, &addr)<<endl;
    cout<<map.searchHashTableRand(1, &addr)<<endl;
}
