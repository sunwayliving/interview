#include <iostream>
using namespace std;

class T{
public:
    T(){
        cout<<"T con" <<endl;
    }
    ~T(){
        cout<<"T decon" <<endl;
    }
};

class M{
public:
    M(){
        cout<<"M con" <<endl;
    }
    M(int){
        cout<<"M con2" <<endl;
    }
    ~M(){
        cout<<"M decon" <<endl;
    }
};

class Base{
public:
    Base(T _t) : t1(_t){
        //这里其实调用了 m2的default constructor 和 copy constructor，然后M(10)的临时对象被销毁
        m2 = M(10);
        cout<<"Base con" <<endl;
        cout<<"**********************construct ends********************" <<endl;
    }
    ~Base(){
        cout<<"Base decon" <<endl;
    }
    int add(int);
    T t1;
    M m1;
    M m2;
};


class X{
public:
    X(){
        cout<<"X Con" <<endl;
    }
    int a;
};

int main()
{
    const int &a = 0;
    cout<<a<<endl;
    return 0;
}
