#include <iostream>
using namespace std;

struct TreeNode{
    int left, right, cover;
    TreeNode *lchild;
    TreeNode *rchild;
    TreeNode(int left, int right, TreeNode *lchild, TreeNode *rchild)
        : left(left), right(right), lchild(lchild), rchild(rchild)
    {
    }
};

TreeNode* build(int l, int r){
    TreeNode *root = new TreeNode(l, r, NULL, NULL);
    if(l + 1 < r){
        int mid = l + ((r - l)>>1);
        root->lchild = build(l, mid);
        root->rchild = build(mid, r);
    }
    return root;
}

void insertInterval(TreeNode *root, int c, int d){
    if(c <= root->left && d >= root->right){
        root->cover++;
    }else{
        if(c < (root->left + root->right) / 2)
            insertInterval(root->lchild, c, d);
        if(d > (root->left + root->right) / 2)
            insertInterval(root->rchild, c, d);
    }
}

void deleteInterval(TreeNode *root, int c, int d){
    if(c <= root->left && d >= root->right){
        root->cover--;
    }else{
        if(c < (root->left + root->right) / 2)
            deleteInterval(root->lchild, c, d);
        if(d < (root->left + root->right) / 2)
            deleteInterval(root->rchildx, c, d);
    }
}

int main()
{

    return 0;
}
