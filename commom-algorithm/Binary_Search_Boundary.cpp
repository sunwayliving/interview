#include <iostream>
#include <cmath>
using namespace std;


//********************************************************************************
//recursive
int binary_search(int a[], const int low, const int high, const int key)
{
    if(low > high) return -1;

    const int mid = low + (high - low) / 2;
    if(key == a[mid]) return mid;

    if(key < a[mid])
        return binary_search(a, low, mid - 1, key);
    else
        return binary_search(a, mid + 1, high, key);
}

//non-recursive
int binary_search_non_recursive(int a[], const int n, const int key)
{
    int low = 0, high = n - 1;
    while(low <= high){
        int mid = low + (high - low) / 2;
        if(a[mid] == key) return mid;

        if(a[mid] < key) low = mid + 1;
        else high = mid - 1;
    }

    return -1;
}

//********************************************************************************
//use BS to search key boundary
//recursive
//https://oj.leetcode.com/problems/search-for-a-range/
int binary_search_right_boundary(int a[], const int low, const int high, const int key)
{
    if(low > high) return -1;

    const int mid = low + (high - low) / 2;

    if(key < a[mid])
        return binary_search_right_boundary(a, low, mid - 1, key);
    else if(key > a[mid])
        return binary_search_right_boundary(a, mid + 1, high, key);
    else{
        int pos = binary_search_right_boundary(a, mid + 1, high, key);
        return pos == -1 ? mid : pos;
    }
}

int binary_search_left_boundary(int a[], const int low, const int high, const int key)
{
    if(low > high) return -1;
    const int mid = low + (high - low) / 2;
    if(a[mid] > key) return binary_search_left_boundary(a, low, mid - 1, key);
    else if(a[mid] < key)  return binary_search_left_boundary(a, mid + 1, high, key);
    else{// equal case
        int pos = binary_search_left_boundary(a, low, mid - 1, key);
        return pos == -1 ? mid : pos;
    }
}

//********************************************************************************
//https://oj.leetcode.com/problems/search-insert-position/
int search_insert_pos(int a[], const int low, const int high, const int key)
{
    if(low > high) return low; //low is exactly the pos that the key should be inserted
    const int mid = low + (high - low) / 2;
    if(a[mid] == key) return mid;
    else if(a[mid] > key) return search_insert_pos(a, low, mid - 1, key);
    else return search_insert_pos(a, mid + 1, high, key);
}

//********************************************************************************
//a run of 0s followed by a run of 1s, find the transition pos
int search_trainsition(int a[], const int n, const int start)
{
    int end = start;
    int p = 1;
    while(end < n && a[end] == a[start]){
        end = start + (int) pow(2, p);
        ++p;
    }
    if(end >= n) return n-1;
    else return search_insert_pos(a, start, end, 1);
}

//binary search to compute the sqrt of x
double binary_search_sqrt(double x)
{
    double l = 0.0, r = x;
    while(true){
        double m = (l + r) / 2.0f;
        if(abs(m *m - x) < 1e-5) return m;
        else if(m * m > x) r = m;
        else l = m;
    }
}

int main()
{
    int a[] = {0, 0, 0, 0, 1, 1, 1};
    const int n = sizeof(a) / sizeof(a[0]);
    sort(a, a + n);
    for(int i = 0; i < n; ++i)
        cout<<a[i] <<" ";
    cout<<endl;

    cout<<search_trainsition(a, n, 0) <<endl;
    cout<<binary_search_sqrt(4) <<endl;
    return 0;
}
