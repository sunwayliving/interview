#include <iostream>
#include <cmath>
using namespace std;

int f[10000] = {-1};
int fibdp(int n){
    if(n <= 0) return 0;
    f[0] = 0;
    f[1] = 1;
    for(int i = 2; i <= n; ++i){
        f[i] = f[i - 1] + f[i - 2];
    }
    return f[n];
}


long fib_cache(int n){
    if(f[n] == -1)
        f[n] = fib_cache(n - 1) + fib_cache(n - 2);
    return f[n];
}

int fibrecur(int n){
    if(n <= 0) return 0;
    //initialize
    f[0] = 0;
    f[1] = 1;
    for(int i = 2; i <= n; ++i)
        f[i] = -1;

    return fib_cache(n);//call cache func
}

int fibform(int n){
    double ret = sqrt(5) / 5.0 * (pow((1 + sqrt(5)) / 2.0, n) - pow((1 - sqrt(5)) / 2.0, n));
    return ret;
}

int main()
{
    int a;
    while(cin>>a){
        cout<<fibdp(a)<<endl;
        cout<<fibform(a)<<endl;
    }

    return 0;
}
