#include <iostream>
using namespace std;

int binary_search(int a[], const int low, const int high, const int key)
{
    if(low > high) return -1;

    const int mid = low + (high - low) / 2;
    if(key == a[mid]) return mid;

    if(key < a[mid])
        return binary_search(a, low, mid - 1, key);
    else
        return binary_search(a, mid + 1, high, key);
}

//non-recursive
int binary_search_non_recursive(int a[], const int n, const int key)
{
    int low = 0, high = n - 1;
    while(low <= high){
        int mid = low + (high - low) / 2;
        if(a[mid] == key) return mid;

        if(a[mid] < key) low = mid + 1;
        else high = mid - 1;
    }

    return -1;
}

int main()
{
    int a[] = {1, 2, 5, 4, 2, 8, 7, 9, 10};
    const int n = sizeof(a) / sizeof(a[0]);
    sort(a, a + n);
    for(int i = 0; i < n; ++i)
        cout<<a[i] <<" ";
    cout<<endl;
    cout<<binary_search(a, 0, n, 2)<<endl;
    return 0;
}
