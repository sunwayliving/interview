#include <iostream>
#include <algorithm>
#include <vector>
#include <cmath>
#include <ctime>
#include <cassert>
using namespace std;

int randInt(int low, int high){
    assert(low <= high);
    return low + rand() % (high - low + 1);
}

//n random pick m
vector<int> sampleF2(int n, int m){
    vector<int> ret;
    //random m times, if t not in then insert t, else insert i;
    for(int i = n - m + 1; i <= n; ++i){
        srand((unsigned) time(NULL));
        int t = randInt(1, i);
        vector<int>::iterator pos = std::find(ret.begin(), ret.end(), t);
        if(pos == ret.end()){//not in the vector
            ret.push_back(t);
        }else
            ret.push_back(i);
    }

    for(int i = 0; i < ret.size(); ++i)
        cout<<ret[i] << " ";
    cout<<endl;
    return ret;
}

//random shuffle
void shuffle(int a[], int n){
    for(int i = n - 1; i >= 1; --i){
        srand((unsigned int)time(NULL));
        int t = rand() % i;
        swap(a[i], a[t]);
    }
}

int main()
{
    int a[] = {1, 2, 3, 4, 5, 6, 7, 8, 9};
    shuffle(a, 9);
    for(int i = 0; i < 9; ++i){
        cout<<a[i]<<" ";
    }
    cout<<endl;
    return 0;
}

//n random pick m
vector<int> sampleF2(int n, int m){
    vector<int> ret;
    //random m times, if t not in then insert t, else insert i;
    for(int i = n - m + 1; i <= n; ++i){
        srand((unsigned) time(NULL));
        int t = randInt(1, i);
        vector<int>::iterator pos = std::find(ret.begin(), ret.end(), t);
        if(pos == ret.end()){//not in the vector
            ret.push_back(t);
        }else
            ret.push_back(i);
    }

    for(int i = 0; i < ret.size(); ++i)
        cout<<ret[i] << " ";
    cout<<endl;
    return ret;
}

//random shuffle
void shuffle(int a[], int n){
    for(int i = n - 1; i >= 1; --i){
        srand((unsigned int)time(NULL));
        int t = rand() % i;
        swap(a[i], a[t]);
    }
}

int main()
{
    int a[] = {1, 2, 3, 4, 5, 6, 7, 8, 9};
    shuffle(a, 9);
    for(int i = 0; i < 9; ++i){
        cout<<a[i]<<" ";
    }
    cout<<endl;
    return 0;
}
