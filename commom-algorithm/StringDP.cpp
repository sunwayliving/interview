#include <cstring>
#include <iostream>
using namespace std;

//longest not repeat substring
int lnrs(char *str, int n){
    if(str == NULL || n <= 0) return 0;

    int cache[256];
    for(int i = 0; i < 256; ++i)
        cache[i] = -1;

    int begin = 0, end = 0;
    int max_len = 0;
    for(; end < n; ++end){
        //update begin pointer
        if(cache[str[end]] != -1)
            begin = cache[str[end]] + 1;
        //update end pointer
        cache[str[end]] = end;
        //update max_len
        max_len = max(max_len, end - begin + 1);
    }
    return max_len;
}

int main()
{
    char *str = "abcdabcdefg";
    cout<<lnrs(str, strlen(str))<<endl;
    return 0;
}
