#include <iostream>
#include <vector>
#include <cmath>
using namespace std;

//http://blog.csdn.net/yanghao58686763/article/details/1812418
vector<string> grayCode(int n){
    vector<string> code;
    code.resize(pow(2, n));
    if(n == 1){
        code.push_back(string("0"));
        code.push_back(string("1"));
        return code;
    }

    vector<string> last;
    last = grayCode(n - 1);
    for(int i = 0; i < last.size(); ++i){
        code[i] = "0" + last[i];
        code[last.size() - 1 - i] = last[i] + "1";
    }
    return code;
}

int main()
{
    vector<string> res;
    res = grayCode(2);
    for(int i = 0; i < res.size(); ++i){
        cout<<res[i] <<" ";
    }
    cout<<endl;
    return 0;
}
