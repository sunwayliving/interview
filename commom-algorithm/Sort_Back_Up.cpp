#include <iostream>
#include <math.h>
using namespace std;

void swap(int &a, int &b)
{
    int tmp = a;
    a = b;
    b = tmp;
}

/*********************************************************************/
//左闭右开的Interval
void bubble_sort(int a[], int start, int end)//end-to-start bubble
{
    bool exchange;
    for(int i = start; i < end - 1; ++i){ //最多n-1趟排序就能把所有元素排好
        exchange = false;                 //每次一趟排序开始, reset
        for(int j = end - 1; j > i; --j){
            if(a[j] < a[j-1]){            //发现相邻逆序对，交换
                swap(a[j], a[j-1]);
                exchange = true;
            }
        }
        if(!exchange) return;
    }
}

/*********************************************************************/
//左闭右开的interval
int partition(int a[], const int start, const int end)
{
    //end-1 is the last element
    int l = start, r = end - 1;
    const int pivot_val = a[l];
    while(l < r){
        while(l < r && a[r] >= pivot_val) --r;
        a[l] = a[r];
        while(l < r && a[l] <= pivot_val) ++l;
        a[r] = a[l];
    }
    a[l] = pivot_val; //both l and r are ok
    return l;         //l is pivot position
}

void quick_sort(int a[], int start, int end)
{
    if(end - 1 > start){//at least two elements
        const int pivot_pos = partition(a, start, end);
        quick_sort(a, start, pivot_pos); //右开的区间
        quick_sort(a, pivot_pos + 1, end);
    }
}

int kth_number(int a[], int n, int start, int end, int k)
{
    const int pivot_pos = partition(a, start, end);

    int real_pos = n - k;
    if(pivot_pos == real_pos)
        return a[real_pos];
    else if(pivot_pos > real_pos)
        return kth_number(a, n, 0, pivot_pos - 1, k);
    else
        return kth_number(a, n, pivot_pos + 1, end, k);
}


/*********************************************************************/
void straight_insertion_sort(int a[], const int start, const int end)
{
    int i, j;
    for(i = start + 1; i < end; ++i){
        int tmp = a[i];
        for(j = i - 1; j >= start && tmp < a[j]; --j)
            a[j+1] = a[j];

        a[j+1] = tmp;
    }
}

/*********************************************************************/
void shell_insert(int a[], const int start, const int end, const int gap)
{
    int i,j;
    for(i = start + gap; i < end; i += gap){
        int tmp = a[i];
        for(j = i - gap; j >= start && tmp < a[j]; j -= gap)
            a[j+gap] = a[j];

        a[j+gap] = tmp;
    }
}

void shell_sort(int a[], const int start, const int end)
{
    int gap = end - start;
    while(gap > 1){
        gap = gap / 3 + 1;
        shell_insert(a, start, end, gap);
    }
}

/*********************************************************************/
void simple_selection_sort(int a[], const int start, const int end)
{
    int i, j, min_idx;
    for(i = start; i < end; ++i){
        min_idx = i;
        for(j = i + 1; j < end; ++j)
            min_idx = a[j] < a[min_idx] ? j : min_idx;

        if(min_idx != i) swap(a[i], a[min_idx]);
    }
}

/*********************************************************************/
//二路归并. tmp: 于a等长的辅助数组
void merge(int a[], int tmp[], const int start, const int mid, const int end)
{
    int i, j, k;
    for(i = 0; i < end; ++i) tmp[i] = a[i];

    i = start, j = mid, k = start;
    while(i < mid && j < end){
        if(tmp[i] < tmp[j]){
            a[k] = tmp[i];
            ++k, ++i;
        }else{
            a[k] = tmp[j];
            ++k, ++j;
        }
    }
    while(i < mid) a[k++] = tmp[i++];
    while(j < end) a[k++] = tmp[j++];
}

void merge_sort(int a[], int tmp[], const int start, const int end)
{
    if(end - 1 > start){
        const int mid = start + (end - start) / 2;
        merge_sort(a, tmp, start, mid);
        merge_sort(a, tmp, mid, end);
        merge(a, tmp, start, mid, end);
    }
}

/*********************************************************************/
//a的数值范围：0 ~ k
void countingSort(int A[], const int n, const int k)
{
    int *count = (int*) malloc(sizeof(int) * (k + 1));
    for(int i = 0; i < k+1; ++i)
        count[i] = 0;

    //count the count of array
    for(int i = 0; i < n; ++i)
        count[A[i]]++;
    //add up the count
    for(int i = 1; i < k+1; ++i)
        count[i] += count[i-1];

    int *B = (int*) malloc(sizeof(int) * n);
    //reverse traversal for stability, take {0, 1, 2, 2}
    //if from begin to end, the first "2" will be put at the end of B
    for(int i = n - 1; i >= 0; --i){
        B[count[A[i]] - 1] = A[i];
        count[A[i]]--;  //for duplicate numbers, reduce the count
    }

    //copy back to A
    for(int i = 0; i < n; ++i)
        A[i] = B[i];

    free(B);
    free(count);
}

/*********************************************************************/
//radix sort
//n: a中记录的个数
//k: a中元素的最大值
void radixCountingSort(int a[], int n, int k, int d)
{
    int *count = (int*)malloc(sizeof(int) * (k+1));
    int *b = (int*)malloc(sizeof(int) * n);
    for(int i = 0; i < k+1; ++i)
        count[i] = 0;

    //统计a[i]在第d位上的记录的个数
    cout<<"here"<<endl;
    for(int i = 0; i < n; ++i){
        int digit = (a[i] / (int)(pow(10, d-1))) % 10;
        cout<<digit<<endl;
        count[digit]++;
    }

    //add up
    for(int i = 1; i < k+1; ++i)
        count[i] += count[i-1];

    //sort
    for(int i = n-1; i >= 0; --i){
        int digit = (a[i] / (int)pow(10, d-1)) % 10;
        //attention count[digit] - 1: real position index, use count[0] to think
        b[count[digit]-1] = a[i];
        count[digit]--;
    }

    for(int i = 0; i < n; ++i)
        a[i] = b[i];

    free(count);
    free(b);
}

//a中元素最大有d位, 每一位都是0~9的数字
void radixSort(int a[], int n, int d)
{
    for(int i = 1; i <= d; ++i)
        radixCountingSort(a, n, 9, i);
}



/*********************************************************************/
void printArray(int a[], int n)
{
    for(int i = 0; i < n; ++i)
        cout<<a[i]<<" ";
    cout<<endl;
}

int main()
{
    int a[] = {123, 2, 120, 284, 25, 6, 238, 17,279, 1 };
    const int n = sizeof(a) / sizeof(a[0]);
    int *tmp = (int*)malloc(n * sizeof(int));
    radixSort(a, n, 3);
    printArray(a, n);
    return 0;
}
