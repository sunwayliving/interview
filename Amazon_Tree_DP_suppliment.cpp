#include <iostream>
#include <vector>

using namespace std;

int main()
{
    int m, n;
    cin>>m>>n;
    vector<vector<int> > cargo(m, vector<int>(n, 0));
    for(int i = 0; i < m; ++i){
        for(int j = 0; j < n; ++j){
            cin>>cargo[i][j];
        }
    }

    int min_cost = 10000000;
    int min_row = 0, min_col = 0;
    int i, j;
    for(i = 0; i < m; ++i){
        for(j = 0; j < n; ++j){
            //for every supplier
            int cost = 0;
            for(int p = 0; p < m; ++p){
                for(int q = 0; q < n ; ++q){
                    cost += (abs(p - i) + abs(q - j)) * cargo[p][q];
                }
            }
            if(cost < min_cost){
                min_cost = cost;
                min_row = i;
                min_col = j;
            }
        }
    }
    cout<< min_row <<" " <<min_col;
    return 0;
}
