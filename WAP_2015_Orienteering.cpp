#include <iostream>
#include <vector>
#include <string>
#include <queue>

using namespace std;

//definition ************************************************************
class Orienteering {
    struct Point{
        int x, y;
        Point(int _x = -1, int _y = -1) : x(_x), y(_y){}
    };

public:
    void main();
    void readTerrain();
    void computeDistance();
    void dp();
    //for debug
    friend void printMatrix(vector<vector<int> > &matrix);

private:
    int bfs(Point &start, Point &end);

private:
    vector<string> m_terrain;
    vector<Point> m_city;
    vector<vector<int> > m_dist;
    Point m_sp;
    Point m_ep;
    int m_width;
    int m_height;
};

//************************************************************
//for debug
void printMatrix(vector<vector<int> > &matrix){
    for(size_t i = 0; i < matrix.size(); ++i){
        for(size_t j = 0; j < matrix[i].size(); ++j){
            cout<<matrix[i][j] <<" ";
        }
        cout<<endl;
    }
}

//implementation ************************************************************
void Orienteering::main(){
    readTerrain();
    computeDistance();
    printMatrix(m_dist);
    dp();
}

void Orienteering::readTerrain(){
    m_terrain.clear();
    cin>>m_width>>m_height;
    vector<Point> v_city;
    string line;
    for(int i = 0; i < m_height; ++i){
        cin>>line;
        m_terrain.push_back(line);
        for(size_t j = 0; j < line.length(); ++j){
            if(line[j] == 'S') { m_sp = Point(i, j); }
            if(line[j] == 'G') { m_ep = Point(i, j); }
            if(line[j] == '@') { m_city.push_back(Point(i, j)); }
        }
    }
    m_city.push_back(m_sp);
    swap(m_city.back(), m_city.front());
    m_city.push_back(m_ep);

    //pre-allocate memory for distance
    int city_size = m_city.size();
    m_dist = vector<vector<int> >(city_size, vector<int>(city_size, 0));
}

void Orienteering::computeDistance(){
    for(size_t i = 0; i < m_city.size(); ++i){
        for(size_t j = i + 1; j < m_city.size(); ++j){
            int w = bfs(m_city[i], m_city[j]);
            m_dist[i][j] = m_dist[j][i] = w;
        }
    }
}

int Orienteering::bfs(Point &start, Point &end){
    vector<vector<int> > visited(m_height, vector<int>(m_width, 0));
    int dir[4][2] = {{0, 1}, {0, -1}, {1, 0}, {-1, 0}};

    queue<Point> que;
    que.push(start);
    visited[start.x][start.y] = 1;
    int step = 0;
    while(!que.empty() && visited[end.x][end.y] == 0){
        int level_size = que.size();
        step++;
        for(int i = 0; i < level_size && visited[end.x][end.y] == 0; ++i){
            Point temp = que.front();
            que.pop();

            for(int d = 0; d < 4; ++d){
                Point extend(temp.x + dir[d][0], temp.y + dir[d][1]);
                if(extend.x >= 0 && extend.x < m_height && extend.y >= 0 && extend.y < m_width
                   && m_terrain[extend.x][extend.y] != '#' && visited[extend.x][extend.y] == 0){
                    que.push(extend);
                    visited[extend.x][extend.y] = 1;
                }
            }//end for d
        }//end for i
    }//end while
    if(visited[end.x][end.y] == 1) return step;
    else return INT_MAX;
}

void Orienteering::dp(){
    int ans = INT_MAX;
    int n = m_city.size();
    n = n - 1;//exclude the start point
    vector<vector<int> > dp((1 << n), vector<int>(n + 1));
    for(int i = 0; i < (1 << n); ++i){
        for(int j = 1; j < n + 1; ++j){
            if(i == (1 << (j - 1))){
                dp[i][j] = m_dist[0][j];
            }else{
                if(i & (1 << (j - 1))){
                    dp[i][j] = INT_MAX;
                    for(int k = 1; k < n + 1; ++k){
                        if(k != j && (i & (1 << (k - 1)))){
                            dp[i][j] = min(dp[i][j], dp[i ^ (1 << (j - 1))][k] + m_dist[k][j]);
                        }
                    }
                }
            }//end else
        }//end for j
    }//end for i

    for(int i = 1; i < n + 1; ++i){
        ans = min(ans, dp[(1 << n) - 1][i] + m_dist[i][m_city.size() - 1]);
    }

    cout<<ans<<endl;
    return;
}

int main()
{
    Orienteering o;
    o.main();
    return 0;
}
