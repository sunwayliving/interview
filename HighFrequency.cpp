#include <iostream>
#include <cstring>
#include <cstdio>
#include <cmath>
#include <cassert>
#include <ctime>
#include <vector>
#include <sstream>
#include <algorithm>
using namespace std;
//二分搜索的前提！！！！！
//数组已经排好序！！！！
bool bs(int a[], int start, int end, int key){
    while(start < end){
        int mid = start + (end - start) / 2;
        if(a[mid] == key){
            return true;
        }else if(a[mid] < key){
            start = mid + 1;
        }else if(a[mid] > key){
            end = mid;
        }
    }
    return false;
}

bool tue(){
    return false;
}

bool bsrecur(int a[], int start, int end, int key){
    assert(tue());
    if(start >= end) return false;
    int mid = start + (end - start) / 2;
    if(a[mid] == key) return true;
    else if(a[mid] < key){
        return bs(a, mid + 1, end, key);
    }else if (a[mid] > key){
        return bs(a, start, mid, key);
    }
}


int lcs(int a[], int n){
    int max = 0;
    for(int i = 1; i < n; ++i){
        if(a[i-1] >= 0) a[i] += a[i-1];
        if(a[i] > max) max = a[i];
    }
    return max;
}

int lcs(int a[], int start, int end){
    if(start >= end) return 0; //zero element
    if(start + 1 == end) return max(0, a[start]);//one element

    int mid = start + (end - start) / 2;
    int lmax, rmax, sum;
    //find max left;
    sum = lmax = 0;
    for(int i = mid - 1; i >= start; --i){
        sum += a[i];
        lmax = max(lmax, sum);
    }
    //find max right
    sum = rmax = 0;
    for(int i = mid; i < end; ++i){
        sum += a[i];
        rmax = max(rmax, sum);
    }

    return max(lmax + rmax, max(lcs(a, start, mid), lcs(a, mid + 1, end)));
}

void insertSort(int a[], int start, int end){
    int i, j;
    for(i = start + 1; i < end; ++i){
        int tmp = a[i];
        for(j = i - 1; j >= 0 && tmp < a[j]; --j)
            a[j+1] = a[j];
        a[j+1] = tmp;
    }
}

int partition(int a[], int start, int end){
    swap(a[start], a[start + rand() % (end - start)]);
    int pivot_val = a[start];
    int l = start, r = end - 1;
    while(l < r){
        while(l < r && a[r] > pivot_val) r--;
        a[l] = a[r];
        while(l < r && a[l] < pivot_val) l++;
        a[r] = a[l];
    }
    a[l] = pivot_val;
    return l;
}

void quickSort(int a[], int start, int end){
    if(start + 1 < end){
        int pivot_pos = partition(a, start, end);
        quickSort(a, start, pivot_pos);
        quickSort(a, pivot_pos + 1, end);
    }
}

void getKnuth(int n, int m){
    for(int i = 0; i < n; ++i){
        if(rand() % ( n - i ) < m){
            cout<< i <<" ";
            --m;
        }
    }
}

void getNext(char pat[], int next[]){
    const int m = strlen(pat);
    next[0] = -1;
    int i, j = -1;
    for(i = 1; i < m; ++i){
        while(j > -1 && pat[i] != pat[j+1]) j = next[j];
        if(pat[i] == pat[j+1]) j++;
        next[i] = j;
    }
}

void kmp(char txt[], char pat[]){
    int *next = (int*) malloc(sizeof(int) * strlen(pat));
    getNext(pat, next);
    const int n = strlen(txt);
    const int m = strlen(pat);
    int i, j = -1;
    for(i = 0; i < n; ++i){
        while(j > -1 && txt[i] != pat[j+1]) j = next[j];
        if(txt[i] == pat[j+1]) j++;
        if(j == m - 1) {
            cout<< "found: " << i - j + 1 <<" ";
        }
    }
}


class Person{
public:
    //explicit Person(int a) : m_age(a){
    //};

    Person(const Person &other){
        m_age = other.m_age;
    }

    void sayAge(){
        cout<<m_age<<endl;
    }
private:
    int m_age;
};

bool isEven(int n){
    return (n & 0x1) == 0;
}

void switchNum(int a[], int start, int end, bool (*pf)(int n)){
    int l = start;
    int r = end - 1;
    int pivot_val = a[l];
    while(l < r){
        while(l < r && pf(a[r])) r--;
        a[l] = a[r];
        while(l < r && !pf(a[l])) l++;
        a[r] = a[l];
    }
    a[l] = pivot_val;
}

int uglyNum(int n){
    if(n <= 0) return 0;
    int *num = (int *) malloc(sizeof(int) * (n + 1));
    num[0] = 0;
    num[1] = 2;
    num[2] = 3;
    num[3] = 5;
    int twoIdx = 1, threeIdx = 1, fiveIdx = 1;
    int minIdx = 0;
    for(int i = 4; i <= n; ++i){
        while(num[twoIdx] * 2 <= num[i - 1])
            twoIdx++;
        while(num[threeIdx] * 3 <= num[i - 1])
            threeIdx++;
        while(num[fiveIdx] * 5 <= num[i - 1])
            fiveIdx++;
        num[i] = min(num[twoIdx] * 2, min(num[threeIdx] * 3, num[fiveIdx] * 5));
    }
    int tmp = num[n];
    free(num);
    return tmp;
}

void twoSum(int a[], int start, int end, int sum){
    if(a == NULL) return ;

    for(int i = start; i < end; ++i){
        if(bs(a, start, end, sum - a[i])){
            cout<<a[i] <<" " <<sum - a[i] <<endl;
            break;
        }
    }
}

void twoSum2(int a[], int start, int end, int sum){
    int l = start;
    int r = end - 1;
    while(l < r){
        if(a[l] + a[r] == sum){
            cout<<a[l] <<" " <<a[r] <<endl;
            r--;
            //break;
        }else if(a[l] + a[r] > sum){
            r--;
        }else {
            l++;
        }
    }
}

void continuesSum(int a[], int start, int end, int sum){
    int l = start;
    int r = l + 1;
    int csum = a[l] + a[r];
    while(l < end && r < end && l < r){
        if(csum == sum){
            for(int i = l; i <= r; ++i)
                cout<<a[i] <<" ";
            break;
        }else if (csum > sum){
            csum -= a[l];
            l++;
        }else{
            r++;
            csum += a[r];
        }
    }
}

void rot(string &s, int n){
    reverse(s.begin(), s.begin() + n);
    reverse(s.begin() + n, s.end());
    reverse(s.begin(), s.end());
}

void printP(int diceNum, int g_maxVal = 6){
    if(diceNum < 1) return;

    int* prob[2];
    prob[0] = new int[diceNum * g_maxVal + 1];
    prob[1] = new int[diceNum * g_maxVal + 1];
    for(int i = 0; i <= diceNum * g_maxVal; ++i){
        prob[0][i] = 0;
        prob[1][i] = 0;
    }

    int flag = 0;
    for(int i = 1; i <= g_maxVal; ++i)
        prob[flag][i] = 1;

    for(int k = 2; k <= diceNum; ++k){
        for(int i = 0; i < k; ++i)//lower than k is impossible
            prob[1 - flag][i] = 0;

        for(int i = k; i <= k * g_maxVal; ++i){
            prob[1 - flag][i] = 0;
            for(int j = 1; j <= i && j <= g_maxVal; ++j)
                prob[1 - flag][i] += prob[flag][i - j];
        }
        flag = 1 - flag;
    }

    int total = pow(g_maxVal, diceNum);
    //prob[flag][diceNum], prob[flag][diceNum * g_maxVal]
    for(int i = 0; i < 3; ++i){
        double max = prob[flag][diceNum];
        int maxIdx = diceNum;
        int j = 0;
        for(j = diceNum; j <= diceNum * g_maxVal; ++j){
            if(prob[flag][j] > max){
                max = prob[flag][j];
                maxIdx = j;
            }
        }
        prob[flag][maxIdx] = 0.0f;
        printf("%d %.2f\n", maxIdx, max * 1.0f / total);
    }

    delete[] prob[0];
    delete[] prob[1];
}

int myAdd(int a, int b){
    return (a & b + ((a ^ b)>>1))<<1;
}

bool isPrime(int n){
    if(n <= 1) return false;

    for(int i = 2; i * i <= n; ++i ){
        if( n % i == 0)
            return false;
    }
    return true;
}

int randInt(int low, int high){
    assert(low <= high);
    srand((unsigned int)time(NULL));
    return low + rand() % (high - low + 1);
}

//n random pick m
vector<int> sampleF2(int n, int m){
    vector<int> ret;
    //random m times, if t not in then insert t, else insert i;
    for(int i = n - m + 1; i <= n; ++i){
        srand((unsigned) time(NULL));
        int t = randInt(1, i);
        vector<int>::iterator pos = std::find(ret.begin(), ret.end(), t);
        if(pos == ret.end()){//not in the vector
            ret.push_back(t);
        }else
            ret.push_back(i);
    }

    for(int i = 0; i < ret.size(); ++i)
        cout<<ret[i] << " ";
    cout<<endl;
    return ret;
}

//random shuffle
void shuffle(int a[], int n){
    for(int i = n - 1; i >= 1; --i){
        srand((unsigned int)time(NULL));
        int t = rand() % i;
        swap(a[i], a[t]);
    }
}

double newton(double n){
    double x = 10000000.0;
    while(abs(x * x - n) > 1e-6){
        x = (x + n / x) / 2.;
        cout<<x <<endl;
    }
    return x;
}

int kth_partition(int a[], int start, int end){
    int l = start, r = end - 1;
    //swap(a[l], a[l + ((r - l)>>1)]);
    swap(a[l], a[randInt(l, r-1)]);

    int pivot_val = a[l];
    while(l < r){
        while(l < r && a[r] >= pivot_val) r--;
        a[l] = a[r];
        while(l < r && a[l] <= pivot_val) l++;
        a[r] = a[l];
    }
    a[l] = pivot_val;
    return l;
}

void qs(int a[], int start, int end){
    if(start + 1 < end){
        int pivot_pos = kth_partition(a, start, end);
        qs(a, start, pivot_pos);
        qs(a, pivot_pos + 1, end);
    }
}

int kth_par(int a[], int n, int k){
    assert(k >= 1);
    k = k - 1;

    int pivot_pos = 0;
    int l = 0, r = n;
    while(l < r){
        pivot_pos = kth_partition(a, l, r);
        if(pivot_pos == k) return a[k];
        else if(pivot_pos > k){
            r = pivot_pos;
        }else{
            l = pivot_pos + 1;
        }
    }
}

int par(int a[], int start, int end){
    swap(a[start], a[start + ((end - start)>>1)]);
    const int pivot_val = a[start];
    int l = start, r = end - 1;
    while(l < r){
        while(l < r && a[r] >= pivot_val) r--;
        a[l] = a[r];
        while(l < r && a[l] <= pivot_val) l++;
        a[r] = a[l];
    }
    a[l] = pivot_val;
    return l;
}

int kth(int a[], int n, int k){
    assert(k >= 0);
    k -= 1;
    int l = 0, r = n;
    while(l < r){
        int pivot_pos = par(a, l, r);
        if(pivot_pos == k){
            return a[k];
        }else if(pivot_pos < k){
            l = pivot_pos + 1;
        }else {
            r = pivot_pos;
        }
    }
}

unsigned int reverseBin(unsigned int n){
    unsigned int ret = 0;
    for(int i = 0; i < 32; ++i, n >>= 1){
        ret <<= 1;
        ret |= n & 0x1;
    }
    return ret;
}

void find(int *a, int n){
    if(a == NULL || n <= 0) return ;
    int min_val, max_val;
    if(n == 1){
        min_val = max_val = a[0];
    }else{
        min_val = min(a[0], a[1]);
        max_val = max(a[0], a[1]);
        for(int i = 2; i + 1< n; i += 2){
            if(a[i] < a[i + 1]){
                min_val = min(min_val, a[i]);
                max_val = max(max_val, a[i + 1]);
            }else{
                min_val = min(min_val, a[i + 1]);
                max_val = max(max_val, a[i]);
            }
        }
        //unite the even and odd
        min_val = min(min_val, a[n - 1]);
        max_val = max(max_val, a[n - 1]);
        cout<<"min: " <<min_val <<endl;
        cout<<"max: " <<max_val <<endl;
    }
}

int bs1(int *a, int start, int end, int val){
    if(a == NULL) return -1;
    while(start < end){
        int mid = start + ((end - start) >> 1);
        if(a[mid] < val){
            start = mid + 1;
        }else if(a[mid] > val){
            end = mid;
        }else{
            return mid;
        }
    }
    return -1;
}

int bs2(int *a, int start, int end, int val){
    if(a == NULL) return -1;
    while(start < end){
        int mid = start + ((end - start) >> 1);
        if(a[mid] < val){
            start = mid + 1;
        }else if(a[mid] > val){
            end = mid;
        }else{
            end = mid;
        }
    }
    return start;
}

int bs3(int *a, int start, int end, int val){
    if(a == NULL) return 0;
    while(start < end){
        int mid = start + ((end - start) >> 1);
        if(a[mid] < val){
            start = mid + 1;
        }else if(a[mid] > val){
            end = mid;
        }else{
            start = mid + 1;
        }
    }
    return start - 1;
}

int bs4(int *a, int start, int end, int val){
    if(a == NULL) return 0;
    while(start < end){
        int mid = start + ((end - start) >> 1);
        if(a[mid] < val){
            start = mid + 1;
        }else if (a[mid] > val){
            end = mid;
        }else{
            end = mid;
        }
    }
    if(a[start] == val){
        if(start > 0) return start - 1;
        else return -1;
    }else if (a[start] < val){
        return start;
    }else {
        return -1;
    }
}

int bs5(int *a, int start, int end, int val){
    if(a == NULL) return -1;
    int l = start, r = end;
    while(l < r){
        int m = l + ((r -l) >> 1);
        if(a[m] < val){
            l = m + 1;
        }else if (a[m] > val) {
            r = m;
        }else {
            l = m + 1;
        }
    }

    if(l >= end) return -1;
    else if (a[l] == val){
        if(l < end - 1) return l + 1;
        else return -1;
    }else if (a[l] > val){
        return l;
    }else if (a[l] < val){
        return -1;
    }
}

// int a[] = {0, 1, 2, 3,  4, 5, 6, 7, 8, 8, 8, 9};//11
// const int len = sizeof(a) / sizeof(a[0]);
// cout<<bs2(a, 0, len, 3)<<endl;
// cout<<bs3(a, 0, len, 3) <<endl;
// cout<<bs4(a, 0, len, 10) <<endl;
// cout<<bs5(a, 0, len, -1) <<endl;
template<class t>
class array{
public:
    array(int size);
    size_t getVectorSize() {return _data.size();}
    size_t getSize() {return _size;}
private:
    vector<t>  _data;
    size_t  _size;
};
template<class t>
array<t>::array(int size):_size(size),_data(_size)
{ }

int main()
{
    array<int>*arr= new array<int>(3);
    cout<<arr->getVectorSize()<<endl;
    cout<<arr->getSize()<<endl;
    return 0;
}
